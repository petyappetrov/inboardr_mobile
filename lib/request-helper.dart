import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

class RequestHelperBase {
  final String baseUrl;

  String _token = '';

  RequestHelperBase({
    this.baseUrl = 'http://192.168.0.104:3001',
  });

  Map<String, String> _getHeaders() {
    if (_token.isNotEmpty) {
      return { HttpHeaders.authorizationHeader: 'Bearer ' + _token };
    }
    return {};
  }

  Uri _getUri(url) {
    Uri _uri = Uri.parse(baseUrl + url);
    return _uri;
  }

  void setToken(String value) {
    _token = value;
  }

  void removeToken() {
    _token = '';
  }
}

class RequestHelper extends RequestHelperBase {
  Future<dynamic> get(String url) async {
    final response = await http.get(
      _getUri(url),
      headers: _getHeaders()
    );
    return jsonDecode(response.body);
  }

  Future<dynamic> post(String url, [dynamic body]) async {
    http.Response response = await http.post(
      _getUri(url),
      body: body,
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json;charset=utf-8',
        ..._getHeaders(),
      },
    );
    return jsonDecode(response.body);
  }

  Future<dynamic> put(String url, [dynamic body]) async {
    http.Response response = await http.put(
      _getUri(url),
      body: body,
      headers: {
        HttpHeaders.contentTypeHeader: 'application/json;charset=utf-8',
        ..._getHeaders(),
      },
    );
    return jsonDecode(response.body);
  }

  Future<dynamic> multipartRequest(String url, List<http.MultipartFile> files) async {
    http.MultipartRequest request = http.MultipartRequest('POST', _getUri(url), );
    request.headers['Authorization'] = 'Bearer ' + _token;
    request.files.addAll(files);
    http.Response response = await http.Response.fromStream(await request.send());
    return jsonDecode(response.body);
  }
}

final RequestHelper requestHelper = RequestHelper();
