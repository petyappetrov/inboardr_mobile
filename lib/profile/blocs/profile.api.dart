import 'package:inboardr_mobile/profile/blocs/profile.models.dart';
import 'package:inboardr_mobile/request-helper.dart';

class ProfileAPIClient {
  Future<Profile> getProfile() async {
    dynamic response = await requestHelper.get('/profile');
    return Profile.fromJson(response);
  }
}
