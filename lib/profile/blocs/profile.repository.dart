import 'package:flutter/material.dart';
import 'package:inboardr_mobile/profile/blocs/profile.api.dart';
import 'package:inboardr_mobile/profile/blocs/profile.models.dart';

class ProfileRepository {
  final ProfileAPIClient profileAPIClient;

  ProfileRepository({ @required this.profileAPIClient })
    : assert(profileAPIClient != null);

  Future<Profile> getProfile() async {
    return await profileAPIClient.getProfile();
  }
}
