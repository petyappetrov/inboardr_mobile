import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class Profile extends Equatable {
  final String id;
  final String firstName;
  final String lastName;
  final String phone;
  final String avatar;

  Profile({
    @required this.id,
    @required this.firstName,
    @required this.lastName,
    @required this.phone,
    this.avatar,
  });

  @override
  List<Object> get props => [id, firstName, lastName, phone];

  static Profile fromJson(Map<String, dynamic> json) {
    return Profile(
      id: json['id'],
      firstName: json['firstName'],
      lastName: json['lastName'],
      phone: json['phone'],
    );
  }
}
