import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:inboardr_mobile/profile/blocs/profile.event.dart';
import 'package:inboardr_mobile/profile/blocs/profile.models.dart';
import 'package:inboardr_mobile/profile/blocs/profile.repository.dart';
import 'package:inboardr_mobile/profile/blocs/profile.states.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  final ProfileRepository profileRepository;

  ProfileBloc({@required this.profileRepository})
    : assert(profileRepository != null);

  @override
  ProfileState get initialState => ProfileNotLoadedState();

  @override
  Stream<ProfileState> mapEventToState(ProfileEvent event) async* {
    if (event is GetProfileEvent) {
      yield ProfileLoadingState();
      try {
        final Profile profile = await profileRepository.getProfile();
        yield ProfileLoadedState(profile: profile);
      } catch (error) {
        yield ProfileLoadingErrorState(message: error.toString());
      }
    }
  }
}
