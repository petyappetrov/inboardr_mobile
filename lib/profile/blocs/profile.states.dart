import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:inboardr_mobile/profile/blocs/profile.models.dart';

abstract class ProfileState extends Equatable {
  @override
  List<Object> get props => [];
}

class ProfileNotLoadedState extends ProfileState {}

class ProfileLoadingState extends ProfileState {}

class ProfileLoadedState extends ProfileState {
  final Profile profile;
  ProfileLoadedState({ @required this.profile }) : assert(profile != null);

  @override
  List<Object> get props => [profile];
}

class ProfileLoadingErrorState extends ProfileState {
  final String message;
  ProfileLoadingErrorState({ this.message });
}
