import 'package:equatable/equatable.dart';

abstract class ProfileEvent extends Equatable {
  ProfileEvent([List props = const []]) : super();
  @override
  List<Object> get props => [];
}

class GetProfileEvent extends ProfileEvent {
  @override
  String toString() => 'GetProfile';
}
