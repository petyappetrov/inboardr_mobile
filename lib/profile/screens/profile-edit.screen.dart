import 'package:flutter/material.dart';
import 'package:inboardr_mobile/profile/screens/pass-edit.screen.dart';
import 'pass-phone-edit.screen.dart';

class ProfileEdit extends StatelessWidget {
  const ProfileEdit({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Color.fromRGBO(242, 243, 246, 1),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(100.0),
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: FlexibleSpaceBar(
            title: Container(
              color: Colors.transparent,
              padding: EdgeInsets.fromLTRB(24, 50, 24, 8),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Редактировать \nпрофиль",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 28,
                        color: Colors.black),
                  ),
                  InkWell(
                      onTap: () {
                        Navigator.pushNamed(context, '/profile');
                      },
                      child: Icon(
                        Icons.notifications,
                        color: Colors.grey,
                        size: 35,
                      ))
                ],
              ),
            ),
          ),
          elevation: 0,
          backgroundColor: Color.fromRGBO(242, 243, 246, 1),
        ),
      ),
      body: Column(
        children: <Widget>[
          CircleAvatar(
            radius: 60,
            backgroundImage: NetworkImage(
                "https://scontent-lga3-1.cdninstagram.com/v/t51.2885-15/e35/75448842_159563661956867_8070427991129482435_n.jpg?_nc_ht=scontent-lga3-1.cdninstagram.com&_nc_cat=111&_nc_ohc=8IvFY8r8XHEAX_TJ-9Y&oh=1c5942255e432bea4b2a7d398cdb817b&oe=5EDA7E21&ig_cache_key=MjE3MzMxOTg1OTc3MjU2MzA4Mw%3D%3D.2"),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(24, 20, 24, 0),
            child: Container(
                color: Colors.transparent,
                height: 40,
                child: TextField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Имя'),
                  // textInputAction: TextInputAction.search,
                )),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(24, 20, 24, 0),
            child: Container(
                color: Colors.transparent,
                height: 40,
                child: TextField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Фамилия'),
                  // textInputAction: TextInputAction.continueAction,
                )),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: ButtonTheme(
              minWidth: 300.0,
              height: 50.0,
              child: RaisedButton(
                onPressed: () {},
                child: Text("Сохранить"),
                textColor: Colors.white,
                color: Colors.lightBlue,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
              ),
            ),
          ),
          Padding(
              padding: EdgeInsets.only(top: 10),
              child: FlatButton(
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return Dialog(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0)),
                          child: Container(
                            height: 240,
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      "Внимание",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                        "Вы можете изменять номер телефона только один раз в день. Вы уверены что хотите изменить номер телефона?"),
                                  ),
                                  SizedBox(
                                    width: 320.0,
                                    child: RaisedButton(
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  PassPhoneEdit()),
                                        );
                                      },
                                      child: Text(
                                        "Изменить",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      color: Colors.red,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                    ),
                                  ),
                                  Padding(
                                      padding: EdgeInsets.only(top: 0),
                                      child: FlatButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Text("Отмена"),
                                        color: Colors.transparent,
                                        textColor: Colors.lightBlue,
                                      )),
                                ],
                              ),
                            ),
                          ),
                        );
                      });
                },
                child: Text("Изменить номер телефона"),
                color: Colors.transparent,
                textColor: Colors.lightBlue,
              )),
          Padding(
              padding: EdgeInsets.only(top: 0),
              child: FlatButton(
                onPressed: () {
                  Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  PassEdit()),
                                        );
                },
                child: Text("Изменить пароль"),
                color: Colors.transparent,
                textColor: Colors.lightBlue,
              )),
          Padding(
              padding: EdgeInsets.only(top: 0),
              child: FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text("Отмена"),
                color: Colors.transparent,
                textColor: Colors.lightBlue,
              )),
          Padding(
              padding: EdgeInsets.only(top: 10),
              child: FlatButton(
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return Dialog(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0)),
                          child: Container(
                            height: 210,
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      "Внимание",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                        "Все ваши данные будут стерты и их будет нельзя восстановить"),
                                  ),
                                  SizedBox(
                                    width: 320.0,
                                    child: RaisedButton(
                                      onPressed: () {},
                                      child: Text(
                                        "Удалить",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      color: Colors.red,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                    ),
                                  ),
                                  Padding(
                                      padding: EdgeInsets.only(top: 0),
                                      child: FlatButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Text("Отмена"),
                                        color: Colors.transparent,
                                        textColor: Colors.lightBlue,
                                      )),
                                ],
                              ),
                            ),
                          ),
                        );
                      });
                },
                child: Text("Удалить профиль"),
                color: Colors.transparent,
                textColor: Colors.red,
              )),
        ],
      ),
    );
  }
}
