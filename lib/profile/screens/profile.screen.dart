import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:inboardr_mobile/profile/blocs/profile.api.dart';
import 'package:inboardr_mobile/profile/blocs/profile.bloc.dart';
import 'package:inboardr_mobile/profile/blocs/profile.event.dart';
import 'package:inboardr_mobile/profile/blocs/profile.models.dart';
import 'package:inboardr_mobile/profile/blocs/profile.repository.dart';
import 'package:inboardr_mobile/profile/blocs/profile.states.dart';
import 'package:inboardr_mobile/ui/top-navigation.dart';
import 'package:inboardr_mobile/ui/user-card.dart';

class ProfileScreenView extends StatelessWidget {
  const ProfileScreenView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        if (
          state is ProfileLoadingState ||
          state is ProfileNotLoadedState
        ) {
          return Container(
            color: Color.fromRGBO(242, 243, 246, 1),
            child: Center(child: CircularProgressIndicator())
          );
        }

        final Profile profile = (state as ProfileLoadedState).profile;
        return SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                TopNavigation(title: 'Профиль'),
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.fromLTRB(24.0, 24, 24.0, 60.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      UserCard(
                        firstName: profile.firstName,
                        lastName: profile.lastName,
                        phone: profile.phone,
                        avatar: profile.avatar,
                      ),
                      SizedBox(height: 24,),
                      _buildHistoryRents()
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildHistoryRents() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'История аренд',
          style: TextStyle(
            color: Colors.black,
            fontSize: 17,
            fontWeight: FontWeight.bold
          ),
        ),
        SizedBox(height: 8),
        Text('У вас пока нет историй аренд'),
      ],
    );
  }
}

class ProfileScreen extends StatelessWidget {
  ProfileScreen({Key key}) : super(key: key);

  final ProfileRepository profileRepository = ProfileRepository(
    profileAPIClient: ProfileAPIClient(),
  );

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ProfileBloc>(
        create: (context) => ProfileBloc(profileRepository: profileRepository)
          ..add(GetProfileEvent()),
        child: ProfileScreenView());
  }
}
