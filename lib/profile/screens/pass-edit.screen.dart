import 'package:flutter/material.dart';

class PassEdit extends StatelessWidget {
  const PassEdit({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Color.fromRGBO(242, 243, 246, 1),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(100.0),
        child: AppBar(
          automaticallyImplyLeading: false,
          flexibleSpace: FlexibleSpaceBar(
            title: Container(
              color: Colors.transparent,
              padding: EdgeInsets.fromLTRB(24, 50, 24, 8),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Изменить пароль",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 28,
                        color: Colors.black),
                  ),
                  InkWell(
                      onTap: () {
                        Navigator.pushNamed(context, '/profile');
                      },
                      child: Icon(
                        Icons.notifications,
                        color: Colors.grey,
                        size: 35,
                      ))
                ],
              ),
            ),
          ),
          elevation: 0,
          backgroundColor: Color.fromRGBO(242, 243, 246, 1),
        ),
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(24, 20, 24, 0),
            child: Container(
                color: Colors.transparent,
                height: 40,
                child: TextField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Введите старый пароль'),
                  // textInputAction: TextInputAction.search,
                )),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(24, 20, 24, 0),
            child: Container(
                color: Colors.transparent,
                height: 40,
                child: TextField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Введите новый пароль'),
                  // textInputAction: TextInputAction.search,
                )),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(24, 20, 24, 0),
            child: ButtonTheme(
              minWidth: 300.0,
              height: 50.0,
              child: RaisedButton(
                onPressed: () {
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(builder: (context) => PhoneEdit()),
                  // );
                },
                child: Text("Изменить"),
                textColor: Colors.white,
                color: Colors.lightBlue,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
              ),
            ),
          ),
          Padding(
              padding: EdgeInsets.only(top: 0),
              child: FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text("Отмена"),
                color: Colors.transparent,
                textColor: Colors.lightBlue,
              )),
        ],
      ),
    );
  }
}
