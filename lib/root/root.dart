import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:inboardr_mobile/orders/screens/orders.screen.dart';
import 'package:inboardr_mobile/root/blocs/destinations/destinations.bloc.dart';
import 'package:inboardr_mobile/root/blocs/destinations/destinations.events.dart';
import 'package:inboardr_mobile/root/blocs/destinations/destinations.models.dart';
import 'package:inboardr_mobile/root/blocs/mode/mode.models.dart';
import 'package:inboardr_mobile/more/screens/more.screen.dart';
import 'package:inboardr_mobile/profile/screens/profile.screen.dart';
import 'package:inboardr_mobile/rentals/screens/rentals.screen.dart';
import 'package:inboardr_mobile/search/screens/search.screen.dart';
import 'package:inboardr_mobile/snowboards/screens/snowboards.screen.dart';
import 'package:inboardr_mobile/ui/bottom-navigation.dart';

class Root extends StatefulWidget {
  final Modes mode;
  final int destination;

  Root({
    Key key,
    @required this.destination,
    @required this.mode,
  }) : super(key: key);

  @override
  _RootState createState() => _RootState();
}

class _RootState extends State<Root> {
  /// Контроллер пагинации главных страниц
  PageController _pageController;

  /// Индекс текущей страницы
  int get destination => widget.destination;

  /// Режим приложения: provider - арендатель, consumer - арендующий
  Modes get mode => widget.mode;

  /// Списки главных страниц арендателя и арендующего
  List<Destination> get destinations {
    switch (mode) {
      case Modes.provider:
        return [
          Destination(
            title: 'Заказы',
            icon: Icons.view_list,
            body: OrdersScreen(key: PageStorageKey('orders-page'))
          ),
          Destination(
            title: 'Сноуборды',
            icon: Icons.terrain,
            body: SnowboardsScreen(key: PageStorageKey('snowboard-page'))
          ),
          Destination(
            title: 'Профиль',
            icon: Icons.account_circle,
            body: ProfileScreen(key: PageStorageKey('profile-page'))
          ),
          Destination(
            title: 'Ещё',
            icon: Icons.more_horiz,
            body: MoreScreen(key: PageStorageKey('more-page'))
          ),
        ];
      default:
        return <Destination>[
          Destination(
            title: 'Поиск',
            icon: Icons.search,
            body: SearchScreen(key: PageStorageKey('search-page'))
          ),
          Destination(
            title: 'Аренды',
            icon: Icons.work,
            body: RentalsScreen(key: PageStorageKey('rentals-page'))
          ),
          Destination(
            title: 'Профиль',
            icon: Icons.account_circle,
            body: ProfileScreen(key: PageStorageKey('profile-page'))
          ),
          Destination(
            title: 'Ещё',
            icon: Icons.more_horiz,
            body: MoreScreen(key: PageStorageKey('more-page'))
          ),
        ];
    }
  }

  /// Инициализация контроллера страниц
  @override
  void initState() {
    super.initState();
    _pageController = PageController(
      initialPage: destination
    );
  }

  /// Проверка и обработка изменения индекса страницы
  @override
  void didUpdateWidget(Root oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.destination != destination) {
      _pageController.jumpToPage(destination);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(242, 243, 246, 1),
      bottomNavigationBar: BottomNavigation(
        destinations: destinations,
        currentIndex: destination,
        setIndex: (int index) {
          BlocProvider.of<DestinationBloc>(context)
            .add(ChangeDestinationEvent(destination: index));
        },
      ),
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: _pageController,
        onPageChanged: (index) {
          BlocProvider.of<DestinationBloc>(context)
            .add(ChangeDestinationEvent(destination: index));
        },
        children: destinations
          .map<Widget>((destination) => destination.body)
          .toList()
      )
    );
  }
}
