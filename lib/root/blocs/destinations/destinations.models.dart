import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class Destination extends Equatable {
  final String title;
  final IconData icon;
  final Widget body;
  final Function bottomButton;

  Destination({
    @required this.title,
    @required this.icon,
    @required this.body,
    this.bottomButton,
  });

  @override
  List<Object> get props => [title, icon, body];
}
