import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class DestinationEvent extends Equatable {
  DestinationEvent([List props = const []]) : super();
  @override
  List<Object> get props => [];
}

class InitializeDestinationEvent extends DestinationEvent {
  @override
  String toString() => 'InitializeModeEvent';
}

class ChangeDestinationEvent extends DestinationEvent {
  final int destination;

  ChangeDestinationEvent({
    @required this.destination,
  });

  @override
  List<Object> get props => [destination];

  @override
  String toString() => 'ChangeDestinationEvent { destination: $destination }';
}
