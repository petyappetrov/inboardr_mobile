import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:inboardr_mobile/root/blocs/destinations/destinations.events.dart';
import 'package:inboardr_mobile/root/blocs/destinations/destinations.states.dart';
import 'package:inboardr_mobile/root/blocs/mode/mode.bloc.dart';
import 'package:inboardr_mobile/root/blocs/mode/mode.states.dart';

class DestinationBloc extends Bloc<DestinationEvent, DestinationState> {
  final ModeBloc modeBloc;
  StreamSubscription modeSubscription;

  DestinationBloc({@required this.modeBloc}) {
    modeSubscription = modeBloc.listen((ModeState state) {
      if (state is ModeLoadedState) {
        add(ChangeDestinationEvent(destination: 0));
      }
    });
  }

  @override
  DestinationState get initialState => DestinationChangedState(destination: 0);

  @override
  Stream<DestinationState> mapEventToState(DestinationEvent event) async* {
    if (event is ChangeDestinationEvent) {
      yield DestinationChangedState(destination: event.destination);
    }
  }

  @override
  Future<void> close() {
    modeSubscription.cancel();
    return super.close();
  }
}
