import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class DestinationState extends Equatable {
  @override
  List<Object> get props => [];
}

class DestinationLoadingState extends DestinationState {}

class DestinationChangedState extends DestinationState {
  final int destination;

  DestinationChangedState({
    @required this.destination
  });

  @override
  List<Object> get props => [destination];
}
