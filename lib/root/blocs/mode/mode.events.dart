import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:inboardr_mobile/root/blocs/mode/mode.models.dart';

abstract class ModeEvent extends Equatable {
  ModeEvent([List props = const []]) : super();
  @override
  List<Object> get props => [];
}

class InitializeModeEvent extends ModeEvent {
  @override
  String toString() => 'InitializeModeEvent';
}

class UpdateModeEvent extends ModeEvent {
  final Modes mode;

  UpdateModeEvent({
    @required this.mode,
  });

  @override
  List<Object> get props => [mode];

  @override
  String toString() => 'UpdateModeEvent { mode: $mode }';
}
