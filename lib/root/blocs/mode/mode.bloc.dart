import 'dart:async';
import 'package:inboardr_mobile/authentication/blocs/authentication.bloc.dart';
import 'package:inboardr_mobile/authentication/blocs/authentication.states.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:inboardr_mobile/root/blocs/mode/mode.models.dart';
import 'package:inboardr_mobile/root/blocs/mode/mode.events.dart';
import 'package:inboardr_mobile/root/blocs/mode/mode.repository.dart';
import 'package:inboardr_mobile/root/blocs/mode/mode.states.dart';

class ModeBloc extends Bloc<ModeEvent, ModeState> {
  final ModeRepository modeRepository;
  final AuthenticationBloc authenticationBloc;
  StreamSubscription authenticationSubscruption;

  ModeBloc({
    @required this.modeRepository,
    @required this.authenticationBloc,
  }) : assert(modeRepository != null) {
    authenticationSubscruption = authenticationBloc.listen((state) {
      if (state is AuthenticationUnauthenticatedState) {
        add(UpdateModeEvent(mode: Modes.consumer));
      }
    });
  }

  @override
  ModeState get initialState => ModeLoadingState();

  @override
  Stream<ModeState> mapEventToState(ModeEvent event) async* {
    if (event is InitializeModeEvent) {
      Modes mode = await modeRepository.getModeFromPersistedStorage();
      yield ModeLoadedState(mode: mode);
    }
    if (event is UpdateModeEvent) {
      await modeRepository.saveModeToPersistedStorage(mode: event.mode);
      yield ModeLoadedState(mode: event.mode);
    }
  }

  @override
  Future<void> close() {
    authenticationSubscruption.cancel();
    return super.close();
  }
}
