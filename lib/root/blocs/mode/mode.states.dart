import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:inboardr_mobile/root/blocs/mode/mode.models.dart';

abstract class ModeState extends Equatable {
  @override
  List<Object> get props => [];
}

class ModeLoadingState extends ModeState {}

class ModeLoadedState extends ModeState {
  final Modes mode;

  ModeLoadedState({
    @required this.mode
  });

  @override
  List<Object> get props => [mode];
}
