import 'package:flutter/material.dart';
import 'package:inboardr_mobile/root/blocs/mode/mode.models.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ModeRepository {
  Future<void> saveModeToPersistedStorage({ @required Modes mode }) async {
    return await SharedPreferences.getInstance()
      ..setString('mode', mode.toString().split('.').last);
  }

  Future<Modes> getModeFromPersistedStorage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String mode = prefs.getString('mode');
    if (mode == null) {
      return Modes.consumer;
    }
    return Modes.values.firstWhere(
      (value) => value.toString() == 'Modes.$mode',
      orElse: () => Modes.consumer
    );
  }

  Future<void> removeModeInPersistedStorage() async {
    await SharedPreferences.getInstance()..remove('mode');
  }
}
