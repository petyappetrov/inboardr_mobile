import 'package:inboardr_mobile/orders/blocs/orders.models.dart';
import 'package:inboardr_mobile/request-helper.dart';

class OrdersAPIClient {
  Future<Order> createOrder(inputs) async {
    dynamic response = await requestHelper.post('/orders', inputs);
    return Order.fromJson(response);
  }

  Future<Order> cancelOrder(String id) async {
    dynamic response = await requestHelper.put('/orders/cancel/$id');
    return Order.fromJson(response);
  }
}
