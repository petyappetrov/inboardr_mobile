import 'package:flutter/material.dart';
import 'package:inboardr_mobile/orders/blocs/orders.api.dart';
import 'package:inboardr_mobile/orders/blocs/orders.models.dart';

class OrdersRepository {
  final OrdersAPIClient ordersAPIClient;

  OrdersRepository({ @required this.ordersAPIClient })
    : assert(ordersAPIClient != null);

  Future<Order> createOrder(inputs) async {
    return await ordersAPIClient.createOrder(inputs);
  }

  Future<Order> cancelOrder(String id) async {
    return await ordersAPIClient.cancelOrder(id);
  }
}
