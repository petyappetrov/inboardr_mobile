import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

enum OrderStatuses {
  pending,
  accepted,
  rejected,
  cancelled,
}

class Order extends Equatable {
  final String id;
  final String comment;
  final String creator;
  final DateTime plannedDate;
  final num sizeBoots;
  final num sizeBoard;
  final num price;
  final OrderStatuses status;

  Order({
    @required this.id,
    @required this.creator,
    @required this.plannedDate,
    @required this.sizeBoots,
    @required this.sizeBoard,
    @required this.price,
    @required this.status,
    this.comment,
  });

  @override
  List<Object> get props => [id, comment, creator];

  static Order fromJson(Map<String, dynamic> json) {
    String status = json["status"];
    return Order(
      id: json['_id'],
      comment: json['comment'],
      creator: json['creator'],
      plannedDate: DateTime.parse(json['plannedDate']),
      sizeBoots: json['sizeBoots'],
      sizeBoard: json['sizeBoard'],
      price: json['price'],
      status: OrderStatuses.values.firstWhere(
        (value) => value.toString() == 'OrderStatuses.$status',
      ),
    );
  }
}
