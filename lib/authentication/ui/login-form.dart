import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:inboardr_mobile/authentication/blocs/authentication.bloc.dart';
import 'package:inboardr_mobile/authentication/blocs/authentication.events.dart';
import 'package:inboardr_mobile/authentication/blocs/authentication.states.dart';
import 'package:inboardr_mobile/authentication/screens/restore-password.screen.dart';

String phonePattern = r'^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$';

class LoginForm extends StatelessWidget {
  final _phoneController = TextEditingController();
  final _passwordController = TextEditingController();
  final _phoneMaskFormatter = new MaskTextInputFormatter(mask: '+7 (###) ###-##-##', filter: { "#": RegExp(r'[0-9]') });
  final _formKey = GlobalKey<FormState>();

  LoginForm({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthenticationBloc, AuthenticationState>(
      listener: (BuildContext context, AuthenticationState state) {
        if (state is AuthenticationAuthenticatedState){
          Navigator.of(context).pop();
        }
        if (state is AuthenticationLoginErrorState) {
          Scaffold.of(context).showSnackBar(SnackBar(content: Text(state.message)));
        }
      },
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 24),
            TextFormField(
              decoration: InputDecoration(labelText: 'Введите номер телефона'),
              controller: _phoneController,
              inputFormatters: [_phoneMaskFormatter],
              validator: (value) {
                if (value.isEmpty) {
                  return 'Пожалуйста заполните поле';
                }
                if (!new RegExp(phonePattern).hasMatch(value)) {
                  return 'Не верный формат';
                }
                return null;
              },
            ),
            TextFormField(
              decoration: InputDecoration(labelText: 'Введите пароль'),
              controller: _passwordController,
              obscureText: true,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Пожалуйста заполните поле';
                }
                if (value.length < 4) {
                  return 'Длина пароля не должно быть меньше 4';
                }
                return null;
              },
            ),
            SizedBox(height: 32),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                RaisedButton(
                  child: Text('Вход'),
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(4.0),
                  ),
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      BlocProvider.of<AuthenticationBloc>(context).add(
                        AuthenticationLoginEvent(
                          phone: _phoneController.text,
                          password: _passwordController.text,
                        ),
                      );
                    }
                  },
                  color: Colors.blue,
                  textColor: Colors.white,
                ),
                SizedBox(width: 8),
                FlatButton(
                  child: Text('Восстановить пароль'),
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(4.0),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => RestorePasswordScreen())
                    );
                  },
                ),
              ],
            ),
          ]
        ),
      )
    );
  }
}
