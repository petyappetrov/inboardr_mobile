import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:inboardr_mobile/authentication/blocs/authentication.bloc.dart';
import 'package:inboardr_mobile/authentication/blocs/authentication.events.dart';
import 'package:inboardr_mobile/authentication/blocs/authentication.states.dart';

String phonePattern = r'^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$';

class RegisterForm extends StatelessWidget {
  final _firstNameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _phoneController = TextEditingController();
  final _passwordController = TextEditingController();
  final _phoneMaskFormatter = new MaskTextInputFormatter(mask: '+7 (###) ###-##-##', filter: { "#": RegExp(r'[0-9]') });
  final _formKey = GlobalKey<FormState>();

  RegisterForm({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthenticationBloc, AuthenticationState>(
      listener: (BuildContext context, AuthenticationState state) {
        if (state is AuthenticationAuthenticatedState){
          Navigator.of(context).pop();
        }
      },
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 24),
            TextFormField(
              decoration: InputDecoration(labelText: 'Введите имя'),
              controller: _firstNameController,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Пожалуйста заполните поле';
                }
                return null;
              },
            ),
            TextFormField(
              decoration: InputDecoration(labelText: 'Введите фамилию'),
              controller: _lastNameController,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Пожалуйста заполните поле';
                }
                return null;
              },
            ),
            TextFormField(
              decoration: InputDecoration(labelText: 'Введите номер телефона'),
              controller: _phoneController,
              inputFormatters: [_phoneMaskFormatter],
              validator: (value) {
                if (value.isEmpty) {
                  return 'Пожалуйста заполните поле';
                }
                if (!new RegExp(phonePattern).hasMatch(value)) {
                  return 'Не верный формат';
                }
                return null;
              },
            ),
            TextFormField(
              decoration: InputDecoration(labelText: 'Введите пароль'),
              controller: _passwordController,
              obscureText: true,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Пожалуйста заполните поле';
                }
                if (value.length < 4) {
                  return 'Длина пароля не должно быть меньше 4';
                }
                return null;
              },
            ),
            SizedBox(height: 32),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                RaisedButton(
                  child: Text('Регистрация'),
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(4.0),
                  ),
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      Scaffold.of(context).showSnackBar(SnackBar(content: Text('Загрузка...')));
                      BlocProvider.of<AuthenticationBloc>(context).add(
                        AuthenticationRegisterEvent(
                          firstName: _firstNameController.text,
                          lastName: _lastNameController.text,
                          phone: _phoneController.text,
                          password: _passwordController.text,
                        ),
                      );
                    }
                  },
                  color: Colors.blue,
                  textColor: Colors.white,
                )
              ],
            ),
            SizedBox(height: 16),
            RichText(
              text: TextSpan(
                style: TextStyle(color: Colors.black, height: 1.6),
                children: [
                  TextSpan(text: 'При нажатии "Регистрация" Вы\u00A0соглашаетесь '),
                  TextSpan(
                    text: 'с\u00A0правилами пользования',
                    style: TextStyle(color: Colors.blue),
                    recognizer: TapGestureRecognizer()..onTap = () => launch('http://petyappetrov.github.io/')
                  ),
                  TextSpan(text: ' и\u00A0'),
                  TextSpan(
                    text: 'политикой конфиденциальности',
                    style: TextStyle(color: Colors.blue),
                    recognizer: TapGestureRecognizer()..onTap = () => launch('http://petyappetrov.github.io/')
                  ),
                ]
              )
            ),
          ]
        )
      )
    );
  }
}
