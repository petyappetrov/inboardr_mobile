import 'package:flutter/material.dart';
import 'package:inboardr_mobile/authentication/screens/register.screen.dart';

class LoginFooter extends StatelessWidget {
  const LoginFooter({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
          SizedBox(height: 120),
          Text('Ты ещё не зарегистрирован?'),
          SizedBox(height: 4),
          GestureDetector(
            child: Text(
              'Выполнить регистрацию',
              style: TextStyle(color: Colors.blue),
            ),
            onTap: () {
              Navigator.push(context,
                MaterialPageRoute(builder: (context) => RegisterScreen())
              );
            },
          ),
          SizedBox(height: 64)
        ]),
      ],
    );
  }
}
