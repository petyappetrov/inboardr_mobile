import 'package:flutter/material.dart';
import 'package:inboardr_mobile/authentication/screens/login.screen.dart';

class RegisterFooter extends StatelessWidget {
  const RegisterFooter({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 120),
            Text('Ты уже зарегистрирован?'),
            SizedBox(height: 4),
            GestureDetector(
              child: Text(
                'Выполнить вход',
                style: TextStyle(color: Colors.blue),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => LoginScreen())
                );
              },
            ),
            SizedBox(height: 64),
          ]),
      ],
    );
  }
}
