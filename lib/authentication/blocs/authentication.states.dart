import 'package:equatable/equatable.dart';

abstract class AuthenticationState extends Equatable {
  @override
  List<Object> get props => [];
}

class AuthenticationLoadingState extends AuthenticationState {}

class AuthenticationAuthenticatedState extends AuthenticationState {}

class AuthenticationUninitializedState extends AuthenticationState {}

class AuthenticationUnauthenticatedState extends AuthenticationState {}

class AuthenticationLoginLoadingState extends AuthenticationState {}

class AuthenticationRegisterLoadingState extends AuthenticationState {}

class AuthenticationLoadingErrorState extends AuthenticationState {
  final String message;
  AuthenticationLoadingErrorState({ this.message });
}

class AuthenticationLoginErrorState extends AuthenticationState {
  final String message;
  AuthenticationLoginErrorState({ this.message });
}

class AuthenticationRegisterErrorState extends AuthenticationState {
  final String message;
  AuthenticationRegisterErrorState({ this.message });
}
