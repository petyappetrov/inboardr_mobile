import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class AuthenticationEvent extends Equatable {
  AuthenticationEvent([List props = const []]) : super();
  @override
  List<Object> get props => [];
}

class AuthenticationApplicationStartedEvent extends AuthenticationEvent {
  @override
  String toString() => 'AuthenticationApplicationStartedEvent';
}

class AuthenticationLoggedOutEvent extends AuthenticationEvent {
  @override
  String toString() => 'LoggedOutEvent';
}

class AuthenticationLoginEvent extends AuthenticationEvent {
  final String phone;
  final String password;

  AuthenticationLoginEvent({
    @required this.phone,
    @required this.password,
  });

  @override
  List<Object> get props => [phone, password];

  @override
  String toString() => 'LoginEvent { phone: $phone, password: $password }';
}

class AuthenticationRegisterEvent extends AuthenticationEvent {
  final String phone;
  final String password;
  final String firstName;
  final String lastName;

  AuthenticationRegisterEvent({
    @required this.phone,
    @required this.password,
    @required this.firstName,
    @required this.lastName,
  });

  @override
  List<Object> get props => [phone, password, firstName, lastName];

  @override
  String toString() => 'RegisterEvent { phone: $phone, password: $password, firstName: $firstName, lastName: $lastName }';
}
