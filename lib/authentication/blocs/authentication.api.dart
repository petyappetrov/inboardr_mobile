import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:inboardr_mobile/request-helper.dart';

class AuthenticationAPIClient {
  Future<String> login({
    @required String phone,
    @required String password,
  }) async {
    dynamic response = await requestHelper.post('/login', jsonEncode({
      'phone': phone,
      'password': password,
    }));
    return response['token'];
  }

  Future<String> register({
    @required String phone,
    @required String password,
    @required String firstName,
    @required String lastName,
  }) async {
    dynamic response = await requestHelper.post('/register', jsonEncode({
      'phone': phone,
      'password': password,
      'firstName': firstName,
      'lastName': lastName,
    }));
    return response['token'];
  }
}
