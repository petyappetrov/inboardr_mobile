import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:inboardr_mobile/authentication/blocs/authentication.api.dart';
import 'package:inboardr_mobile/request-helper.dart';

class AuthenticationRepository {
  final AuthenticationAPIClient authenticationAPIClient;

  AuthenticationRepository({ @required this.authenticationAPIClient });

  Future<void> saveTokenToStorage({ @required String token }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('authentication_token', token);
  }

  Future<void> removeTokenFromStorage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('authentication_token');
  }

  Future<String> getTokenFromStorage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('authentication_token');
    return token ?? '';
  }

  Future<void> addTokenToRequestClient({ @required String token }) async {
    requestHelper.setToken(token);
  }

  Future<void> removeTokenFromRequestClient() async {
    requestHelper.removeToken();
  }

  Future<String> login({
    @required String phone,
    @required String password,
  }) => authenticationAPIClient.login(
    phone: phone,
    password: password
  );

  Future<String> register({
    @required String phone,
    @required String password,
    @required String firstName,
    @required String lastName,
  }) => authenticationAPIClient.register(
    phone: phone,
    password: password,
    firstName: firstName,
    lastName: lastName
  );
}
