import 'dart:async';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:inboardr_mobile/authentication/blocs/authentication.repository.dart';
import 'package:inboardr_mobile/authentication/blocs/authentication.events.dart';
import 'package:inboardr_mobile/authentication/blocs/authentication.states.dart';

class AuthenticationBloc extends Bloc<AuthenticationEvent, AuthenticationState> {
  final AuthenticationRepository authenticationRepository;

  AuthenticationBloc({
    @required this.authenticationRepository,
  }) : assert(authenticationRepository != null);

  @override
  AuthenticationState get initialState => AuthenticationUninitializedState();

  @override
  Stream<AuthenticationState> mapEventToState(AuthenticationEvent event) async* {
    if (event is AuthenticationApplicationStartedEvent) {
      yield* _mapAuthenticationApplicationStartedToState();
    } else if (event is AuthenticationLoggedOutEvent) {
      yield* _mapAuthenticationLoggedOutToState();
    } else if (event is AuthenticationLoginEvent) {
      yield* _mapAuthenticationLoginToState(event);
    } else if (event is AuthenticationRegisterEvent) {
      yield* _mapAuthenticationRegisterToState(event);
    }
  }

  Stream<AuthenticationState> _mapAuthenticationApplicationStartedToState() async* {
    final String token = await authenticationRepository.getTokenFromStorage();
    if (token.isNotEmpty) {
      await authenticationRepository.addTokenToRequestClient(token: token);
      yield AuthenticationAuthenticatedState();
    } else {
      yield AuthenticationUnauthenticatedState();
    }
  }

  Stream<AuthenticationState> _mapAuthenticationLoggedOutToState() async* {
    yield AuthenticationLoadingState();
    await authenticationRepository.removeTokenFromRequestClient();
    await authenticationRepository.removeTokenFromStorage();
    yield AuthenticationUnauthenticatedState();
  }

  Stream<AuthenticationState> _mapAuthenticationLoginToState(AuthenticationLoginEvent event) async* {
    yield AuthenticationLoginLoadingState();
    try {
      String token = await authenticationRepository.login(
        phone: event.phone,
        password: event.password
      );
      await authenticationRepository.addTokenToRequestClient(token: token);
      await authenticationRepository.saveTokenToStorage(token: token);
      yield AuthenticationAuthenticatedState();
    } catch (error) {
      if (error) {
        String message = error.response.data['message'] ?? 'Серверная ошибка';
        yield AuthenticationLoginErrorState(message: message);
      } else {
        yield AuthenticationLoginErrorState(message: error.toString());
      }
    }
  }

  Stream<AuthenticationState> _mapAuthenticationRegisterToState(AuthenticationRegisterEvent event) async* {
    yield AuthenticationRegisterLoadingState();
    try {
      String token = await authenticationRepository.register(
        phone: event.phone,
        password: event.password,
        firstName: event.firstName,
        lastName: event.lastName
      );
      await authenticationRepository.addTokenToRequestClient(token: token);
      await authenticationRepository.saveTokenToStorage(token: token);
      yield AuthenticationAuthenticatedState();
    } catch (error) {
      yield AuthenticationRegisterErrorState(message: error.toString());
    }
  }
}
