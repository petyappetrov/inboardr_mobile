import 'package:flutter/material.dart';
import 'package:inboardr_mobile/authentication/screens/login.screen.dart';
import 'package:inboardr_mobile/authentication/screens/register.screen.dart';

class WelcomeScreen extends StatelessWidget {
  WelcomeScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/welcome-bg.jpg'),
          fit: BoxFit.cover
        )
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Container(
          padding: EdgeInsets.fromLTRB(24.0, 0.0, 24.0, 0.0),
          child: Column(
            children: <Widget>[
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 150),
                    Text(
                      'inBoardr',
                      style: TextStyle(
                        fontSize: 48,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 8),
                    Text(
                      'Добро пожаловать',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 24),
                    Padding(
                      padding: EdgeInsets.only(right: 120),
                      child: Text(
                        'Возьми в\u00A0аренду сноуборд у\u00A0настоящих сноубордистов или начни зарабатывать на\u00A0своем сноуборде.',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w300,
                          height: 1.6,
                        ),
                      ),
                    ),
                    SizedBox(height: 16),
                    Row(
                      children: <Widget>[
                        RaisedButton(
                          child: Text('Регистрация'),
                          color: Colors.blue,
                          textColor: Colors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(4.0),
                          ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => RegisterScreen()),
                            );
                          },
                        ),
                        SizedBox(width: 8),
                        FlatButton(
                          child: Text('Вход'),
                          shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(4.0),
                          ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => LoginScreen()),
                            );
                          },
                        ),
                      ],
                    ),
                  ]
                )
              ),
              Text(
                'Photo by Hamish Duncan on Unsplash',
                style: TextStyle(
                  color: Color.fromRGBO(0, 0, 0, 0.4),
                  fontWeight: FontWeight.w300,
                  fontSize: 10,
                )
              ),
              SizedBox(height: 32),
            ],
          ),
        )
      ),
    );
  }
}
