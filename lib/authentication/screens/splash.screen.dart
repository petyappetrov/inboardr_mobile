import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Image.asset(
        'assets/welcome-bg.jpg',
        width: size.width,
        height: size.height,
        fit: BoxFit.cover,
      )
    );
  }
}
