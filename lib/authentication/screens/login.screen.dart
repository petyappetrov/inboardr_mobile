import 'package:flutter/material.dart';
import 'package:inboardr_mobile/authentication/ui/auth-header.dart';
import 'package:inboardr_mobile/authentication/ui/login-footer.dart';
import 'package:inboardr_mobile/authentication/ui/login-form.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.fromLTRB(24.0, 0.0, 24.0, 0.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              AuthHeader(subtitle: 'Выполнить вход'),
              LoginForm(),
              LoginFooter()
            ],
          ),
        ),
      )
    );
  }
}
