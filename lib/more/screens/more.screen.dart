import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:inboardr_mobile/authentication/blocs/authentication.bloc.dart';
import 'package:inboardr_mobile/authentication/blocs/authentication.events.dart';
import 'package:inboardr_mobile/root/blocs/mode/mode.bloc.dart';
import 'package:inboardr_mobile/root/blocs/mode/mode.events.dart';
import 'package:inboardr_mobile/root/blocs/mode/mode.models.dart';
import 'package:inboardr_mobile/root/blocs/mode/mode.states.dart';
import 'package:inboardr_mobile/ui/top-navigation.dart';

class MoreScreen extends StatelessWidget {
  const MoreScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ModeBloc, ModeState>(
      builder: (context, state) {
        return SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                TopNavigation(title: 'Ещё'),
                Container(
                  padding: EdgeInsets.fromLTRB(24.0, 24, 24.0, 60.0),
                  child: ListView(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    children: <Widget>[
                      (state as ModeLoadedState).mode == Modes.provider
                        ? (
                          _buildLinkButton(label: 'Режим арендующего', onTap: () {
                            HapticFeedback.mediumImpact();
                            BlocProvider.of<ModeBloc>(context).add(UpdateModeEvent(mode: Modes.consumer));
                          })
                        )
                        : (
                          _buildLinkButton(label: 'Режим арендателя', onTap: () {
                            HapticFeedback.mediumImpact();
                            BlocProvider.of<ModeBloc>(context).add(UpdateModeEvent(mode: Modes.provider));
                          })
                        ),
                      _buildLinkButton(label: 'Настройки', onTap: () {}),
                      _buildLinkButton(label: 'Обратная связь', onTap: () {}),
                      _buildLinkButton(label: 'Политика конфиденциальности', onTap: () {}),
                      _buildLinkButton(label: 'Правила использования', onTap: () {}),
                      _buildLinkButton(label: 'Выйти из аккаунта', onTap: () {
                        BlocProvider.of<AuthenticationBloc>(context).add(AuthenticationLoggedOutEvent());
                      }),
                    ],
                  ),
                ),
              ],
            )
          ),
        );
      },
    );
  }

  GestureDetector _buildLinkButton({ String label, Function onTap }) {
    return GestureDetector(
      onTap: onTap,
      behavior: HitTestBehavior.translucent,
      child: Container(
        padding: EdgeInsets.only(bottom: 24, top: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(label, style: TextStyle(fontSize: 18)),
            Icon(Icons.arrow_forward_ios, size: 14)
          ],
        )
      ),
    );
  }
}
