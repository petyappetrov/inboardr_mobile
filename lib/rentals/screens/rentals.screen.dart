import 'package:flutter/material.dart';
import 'package:inboardr_mobile/ui/card.dart';
import 'package:inboardr_mobile/ui/top-navigation.dart';

class RentalsScreen extends StatelessWidget {
  const RentalsScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              TopNavigation(
                title: 'Мои аренды',
              ),
              Container(
                padding: EdgeInsets.fromLTRB(24, 24, 24, 60),
                child: ListView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: <Widget>[
                    CardWithBoxShadow(
                      onTap: () {
                        showModalBottomSheet(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(24.0),
                          ),
                          backgroundColor: Colors.white,
                          isScrollControlled: true,
                          context: context,
                          builder: (context) {
                            return FractionallySizedBox(
                              widthFactor: 1,
                              heightFactor: 0.8,
                              child: Container(
                                child: Center(child: Text('asd')),
                              ),
                            );
                          }
                        );
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(right: 32),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(100.0),
                                  child: Image.network(
                                    'https://i.imgur.com/cVDadwb.png',
                                    width: 70.0,
                                    height: 70.0,
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Святослав Семенов',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18,
                                        height: 1.4),
                                  ),
                                  Text('Burton x Pro',
                                      style: TextStyle(height: 1.4)),
                                  Text('Burton Lite Black',
                                      style: TextStyle(height: 1.4)),

                                ],
                              )
                            ],
                          ),
                          SizedBox(height: 24),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                            Text(
                              'Тариф: 700 руб',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  height: 1.4),
                            ),
                            Text('7 февраля, 2020',
                              style: TextStyle(height: 1.4, fontSize: 16)),
                          ])
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
