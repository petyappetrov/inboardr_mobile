import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:inboardr_mobile/orders/blocs/orders.models.dart';
import 'package:inboardr_mobile/search/blocs/search/search.events.dart';
import 'package:inboardr_mobile/search/blocs/search/search.bloc.dart';
import 'package:inboardr_mobile/search/ui/order-fields.dart';
import 'package:inboardr_mobile/ui/card.dart';

class PendingScreen extends StatefulWidget {
  final Order order;

  PendingScreen({
    Key key,
    @required this.order
  }) : super(key: key);

  @override
  _PendingScreenState createState() => _PendingScreenState();
}

class _PendingScreenState extends State<PendingScreen> {
  String dots = '...';
  Timer timer;

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(Duration(milliseconds: 500), (Timer t) {
      setState(() => dots = dots.length < 3 ? dots + '.' : '');
    });
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(242, 243, 246, 1),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.fromLTRB(24.0, 24.0, 24.0, 80.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CardWithBoxShadow(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Идет поиск$dots',
                      style: TextStyle(
                        fontSize: 26,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 24),
                    OrderFields(order: widget.order),
                    SizedBox(height: 24),
                    SizedBox(
                      width: double.infinity,
                      child: RaisedButton(
                        color: Colors.lightBlue,
                        child: Text('Отменить', style: TextStyle(color: Colors.white)),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4.0),
                        ),
                        onPressed: () {
                          BlocProvider.of<SearchBloc>(context)
                            .add(CancelSearchingEvent(orderId: widget.order.id));
                        },
                      ),
                    ),
                  ],
                ),
              )
            ]
          ),
        ),
      ),
    );
  }
}
