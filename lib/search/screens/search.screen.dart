import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:inboardr_mobile/search/blocs/search/search.events.dart';
import 'package:inboardr_mobile/search/blocs/search/search.models.dart';
import 'package:inboardr_mobile/search/blocs/search/search.bloc.dart';
import 'package:inboardr_mobile/search/blocs/search/search.states.dart';
import 'package:inboardr_mobile/search/screens/offers.screen.dart';
import 'package:inboardr_mobile/search/ui/search-form.dart';
import 'package:inboardr_mobile/ui/top-navigation.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({Key key}) : super(key: key);

  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen>
  with AutomaticKeepAliveClientMixin {

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Navigator(
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute(
          settings: settings,
          builder: (BuildContext context) {
            return BlocListener<SearchBloc, SearchState>(
              listener: (BuildContext context, SearchState state) {
                if (state is StartedSearchingState){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => OffersScreen(
                      order: state.order,
                    ))
                  );
                }
              },
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/welcome-bg.jpg'),
                    fit: BoxFit.cover
                  )
                ),
                child: Scaffold(
                  backgroundColor: Color.fromRGBO(0, 0, 0, 0.1),
                  body: SafeArea(
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                          TopNavigation(
                            title: 'Поиск',
                          ),
                          Container(
                            padding: EdgeInsets.all(24.0),
                            child: SearchForm(
                              onSubmit: (SearchInputs inputs) {
                                BlocProvider.of<SearchBloc>(context)
                                  .add(StartSearchingEvent(inputs: inputs)
                                );
                              },
                            )
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              )
            );
          }
        );
      }
    );
  }
}
