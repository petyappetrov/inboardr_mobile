import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:inboardr_mobile/orders/blocs/orders.models.dart';
import 'package:inboardr_mobile/search/blocs/offers/offers.bloc.dart';
import 'package:inboardr_mobile/search/blocs/offers/offers.models.dart';
import 'package:inboardr_mobile/search/blocs/offers/offers.states.dart';
import 'package:inboardr_mobile/search/blocs/search/search.bloc.dart';
import 'package:inboardr_mobile/search/blocs/search/search.states.dart';
import 'package:inboardr_mobile/search/screens/pending.screen.dart';
import 'package:inboardr_mobile/search/ui/offer-item.dart';

class OffersScreen extends StatelessWidget {
  final Order order;

  OffersScreen({
    Key key,
    @required this.order
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<SearchBloc, SearchState>(
      listener: (BuildContext context, SearchState state) {
        if (state is StoppedSearchingState){
          Navigator.of(context).pop();
        }
      },
      child: BlocBuilder<OffersBloc, OffersState>(
        builder: (BuildContext context, OffersState state) {
          if (state is OffersNotFoundState) {
            return PendingScreen(order: order);
          }
          List<Offer> offers = (state as OffersFoundState).offers;
          return SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.all(24.0),
              width: double.infinity,
              child: ListView(
                padding: EdgeInsets.all(0),
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: offers.map<Widget>((offer) {
                  return OfferItem(offer: offer);
                }),
              ),
            ),
          );
        }

      ),
    );
  }
}
