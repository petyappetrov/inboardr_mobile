import 'package:flutter/material.dart';
import 'package:inboardr_mobile/search/blocs/offers/offers.models.dart';
import 'package:inboardr_mobile/ui/card.dart';

class OfferItem extends StatelessWidget {
  final Offer offer;

  const OfferItem({
    Key key,
    @required this.offer
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CardWithBoxShadow(
      padding: EdgeInsets.all(16.0),
      child: Row(
        children: <Widget>[
          Text('TODO: offer $offer.id')
        ],
      )
    );
  }
}
