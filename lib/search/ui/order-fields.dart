import 'package:flutter/material.dart';
import 'package:inboardr_mobile/orders/blocs/orders.models.dart';
import 'package:intl/intl.dart' as Intl;

class OrderFields extends StatelessWidget {
  final Order order;

  OrderFields({
    Key key,
    @required this.order
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildField(label: 'Дата', value: Intl.DateFormat('d MMMM, EEEE').format(order.plannedDate)),
          _buildField(label: 'Цена', value: order.price.toString() + ' руб.'),
          _buildField(label: 'Размер ботинок', value: order.sizeBoots.toString() + ' EUR'),
          _buildField(label: 'Размер сноубора', value: order.sizeBoard.toString() + ' см'),
        ],
      ),
    );
  }

  Widget _buildField({
    String label,
    @required String value,
  }) {
    return Container(
      padding: EdgeInsets.only(bottom: 4.0),
      child: RichText(
        textAlign: TextAlign.left,
        text: TextSpan(
          style: TextStyle(color: Colors.black),
          children: [
            if (label != null)
              TextSpan(text: '$label: '),
            TextSpan(
              text: '$value',
              style: TextStyle(
                fontWeight: label != null ? FontWeight.bold : FontWeight.normal
              )
            ),
          ]
        ),
      ),
    );
  }
}
