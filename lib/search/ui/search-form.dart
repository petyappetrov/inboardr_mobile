import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:math';
import 'package:flutter/services.dart';
import 'package:inboardr_mobile/search/blocs/search/search.models.dart';
import 'package:intl/intl.dart' as Intl;
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:inboardr_mobile/ui/card.dart';

class SearchForm extends StatefulWidget {
  final Function onSubmit;

  SearchForm({
    @required this.onSubmit,
    Key key
  }) : super(key: key);

  @override
  _SearchFormState createState() => _SearchFormState();
}

class _SearchFormState extends State<SearchForm> {
  /// Контроллер для добавления/редактирования комментария
  final TextEditingController _commentController = TextEditingController();

  /// Выбранный индекс списка из размеров обуви
  int _selectedBootsSizeIndex = 14;

  /// Выбранный индекс списка из роста человека
  int _selectedPersonHeightIndex = 20;

  /// Нужно для скрытия клавиатуры в предложении цены
  final FocusNode _priceKeyboard = FocusNode();

  /// Локальный стейт для предложения цены
  num _price = 500;

  /// Дата аренды, по-умолчению берется сегодняшнее число
  DateTime _plannedDate = DateTime.now();

  /// Список размеров обуви от 34 до 46
  final List<double> bootsSizes = [for(double i = 34; i <= 46; i+= 0.5) i];

  /// Список роста человека от 150 до 185
  final List<int> personHeights = [for(int i = 150; i < 185; i+= 1) i];

  /// Главная фунция поиска предложений
  Function get onSubmit => widget.onSubmit;

  Future _selectDate() async {
    DateTime pickedDate = await showDatePicker(
      locale: Locale('ru'),
      context: context,
      initialDate: _plannedDate,
      firstDate: DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, DateTime.now().hour - 24),
      lastDate: DateTime(DateTime.now().year + 1),
      builder: (context, child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: Colors.black,
            accentColor: Colors.black,
            buttonBarTheme: ButtonBarThemeData(buttonTextTheme: ButtonTextTheme.normal)
          ),
          child: child,
        );
      }
    );
    if (pickedDate != null) {
      setState(() => _plannedDate = pickedDate);
    }
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardActions(
      enable: false,
      tapOutsideToDismiss: true,
      config: KeyboardActionsConfig(
        nextFocus: false,
        actions: [
          KeyboardAction(
            focusNode: _priceKeyboard,
          ),
        ],
      ),
      child: Form(
        child: CardWithBoxShadow(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _buildSelectDateButton(),
              SizedBox(height: 16),
              Row(
                children: <Widget>[
                  _buildSelectSizeButton(context),
                  SizedBox(width: 24),
                  _buildSelectHeightButton(context),
                ],
              ),
              SizedBox(height: 16),
              _buildEnterPriceInput(),
              SizedBox(height: 24),
              SizedBox(
                width: double.infinity,
                height: 50,
                child: RaisedButton(
                  elevation: 0,
                  child: Text('Найти', style: TextStyle(fontSize: 18)),
                  color: Colors.lightGreen,
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4.0),
                  ),
                  onPressed: () {
                    SearchInputs inputs = SearchInputs(
                      plannedDate: _plannedDate.toIso8601String(),
                      sizeBoots: bootsSizes[_selectedBootsSizeIndex],
                      sizeBoard: personHeights[_selectedPersonHeightIndex],
                      price: _price,
                    );
                    onSubmit(inputs);
                  },
                ),
              ),
              SizedBox(height: 12),
              _buildAddCommentButton(context),
            ],
          ),
        )
      ),
    );
  }

  Widget _buildAddCommentButton(context) {
    return GestureDetector(
      onTap: () {
        showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text('Комментарий'),
              content: TextFormField(
                decoration: InputDecoration(hintText: 'Введите текст'),
                maxLines: null,
                autofocus: true,
                keyboardType: TextInputType.multiline,
                controller: _commentController,
              ),
              actions: <Widget>[
                MaterialButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Отмена'),
                ),
                MaterialButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Добавить'),
                )
              ],
            );
          }
        );
      },
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 8),
              child: Text('Добавить комментарий')
            )
          ],
        ),
      ),
    );
  }

  Column _buildEnterPriceInput() {
    TextSpan ts = new TextSpan(
      text: _price.toString(),
      style: TextStyle(fontSize: 32.0, fontWeight: FontWeight.bold)
    );
    TextPainter tp = TextPainter(
      text: ts,
      textDirection: TextDirection.rtl,
    );
    tp.layout();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _buildFormLabel(label: 'Предложите цену'),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              width: max(10, tp.width).toDouble(),
              child: TextFormField(
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(0),
                  border: InputBorder.none,
                ),
                keyboardType: TextInputType.number,
                focusNode: _priceKeyboard,
                initialValue: _price.toString(),
                style: TextStyle(fontSize: 32.0, fontWeight: FontWeight.bold),
                onChanged: (value) => setState(() => _price = num.parse(value)),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Пожалуйста заполните поле';
                  }
                  return null;
                },
              ),
            ),
            Text(' ₽', style: TextStyle(fontSize: 32.0, fontWeight: FontWeight.bold))
          ]
        ),
      ]
    );
  }

  Widget _buildSelectHeightButton(BuildContext context) {
    final FixedExtentScrollController scrollController =
      FixedExtentScrollController(initialItem: _selectedPersonHeightIndex);
    return GestureDetector(
      onTap: () {
        showModalBottomSheet(
          context: context,
          builder: (BuildContext context) {
            return Container(
              height: 280.0,
              child: CupertinoPicker(
                squeeze: 1.1,
                scrollController: scrollController,
                backgroundColor: Colors.white,
                itemExtent: bootsSizes.length.toDouble(),
                onSelectedItemChanged: (int index) =>
                  setState(() => _selectedPersonHeightIndex = index),
                children: List<Widget>.generate(
                  personHeights.length, (int index) {
                    final int height = personHeights[index];
                    return Center(
                      child: Text('$height см'),
                    );
                  }
                )
              ),
            );
          }
        );
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildFormLabel(label: 'Рост'),
          _buildFormValue(value: personHeights[_selectedPersonHeightIndex].toInt().toString() + ' см')
        ]
      )
    );
  }

  Widget _buildSelectSizeButton(BuildContext context) {
    final FixedExtentScrollController scrollController =
      FixedExtentScrollController(initialItem: _selectedBootsSizeIndex);
    return GestureDetector(
      onTap: () {
        showModalBottomSheet(
          context: context,
          builder: (BuildContext context) {
            return Container(
              height: 280.0,
              child: CupertinoPicker(
                squeeze: 1.1,
                scrollController: scrollController,
                backgroundColor: Colors.white,
                itemExtent: bootsSizes.length.toDouble(),
                onSelectedItemChanged: (int index) =>
                  setState(() => _selectedBootsSizeIndex = index),
                children: List<Widget>.generate(
                  bootsSizes.length,
                  (int index) {
                    final double size = bootsSizes[index];
                    return Center(
                      child: Text('$size RU'),
                    );
                  }
                )
              ),
            );
          }
        );
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildFormLabel(label: 'Размер обуви'),
          _buildFormValue(value: bootsSizes[_selectedBootsSizeIndex].toString() + ' RU')
        ],
      ),
    );
  }

  Widget _buildSelectDateButton() {
    return GestureDetector(
      onTap: _selectDate,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildFormLabel(label: 'Дата аренды'),
          _buildFormValue(value: Intl.DateFormat('E, d MMM').format(_plannedDate))
        ],
      ),
    );
  }

  Text _buildFormLabel({ @required String label }) {
    return Text(
      '$label:',
      style: TextStyle(
        fontSize: 20.0,
        fontWeight: FontWeight.w400,
        color: Color.fromRGBO(0, 0, 0, 0.6),
        height: 1.3
      )
    );
  }

  Text _buildFormValue({ @required String value }) {
    return Text(
      value,
      style: TextStyle(
        fontSize: 32.0,
        fontWeight: FontWeight.bold,
        height: 1.3
      )
    );
  }
}
