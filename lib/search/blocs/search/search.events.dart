import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:inboardr_mobile/search/blocs/search/search.models.dart';

abstract class SearchEvent extends Equatable {
  SearchEvent([List props = const []]) : super();
  @override
  List<Object> get props => [];
}

class StartSearchingEvent extends SearchEvent {
  final SearchInputs inputs;

  StartSearchingEvent({ @required this.inputs });

  List<Object> get props => [inputs];

  @override
  String toString() => 'StartSearchingEvent';
}

class CancelSearchingEvent extends SearchEvent {
  final String orderId;

  CancelSearchingEvent({ @required this.orderId });

  List<Object> get props => [orderId];

  @override
  String toString() => 'CancelSearchingEvent';
}
