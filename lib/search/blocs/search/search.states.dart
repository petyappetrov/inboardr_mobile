import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:inboardr_mobile/orders/blocs/orders.models.dart';

abstract class SearchState extends Equatable {
  @override
  List<Object> get props => [];
}

class StoppedSearchingState extends SearchState {}

class StartedSearchingState extends SearchState {
  final Order order;

  StartedSearchingState({ @required this.order });

  List<Object> get props => [order];
}
