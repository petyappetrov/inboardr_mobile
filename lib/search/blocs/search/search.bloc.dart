import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:inboardr_mobile/orders/blocs/orders.models.dart';
import 'package:inboardr_mobile/orders/blocs/orders.repository.dart';
import 'package:inboardr_mobile/search/blocs/search/search.events.dart';
import 'package:inboardr_mobile/search/blocs/search/search.states.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  final OrdersRepository ordersRepository;

  SearchBloc({@required this.ordersRepository})
    : assert(ordersRepository != null);

  @override
  SearchState get initialState => StoppedSearchingState();

  @override
  Stream<SearchState> mapEventToState(SearchEvent event) async* {
    if (event is StartSearchingEvent) {
      yield* _mapStartSearchingToState(event);
    } else if (event is CancelSearchingEvent) {
      yield* _mapCancelSearchingToState(event);
    }
  }

  Stream<SearchState> _mapStartSearchingToState(StartSearchingEvent event) async* {
    final Order order = await ordersRepository.createOrder(
      jsonEncode(event.inputs.toJson())
    );
    yield StartedSearchingState(order: order);
  }

  Stream<SearchState> _mapCancelSearchingToState(CancelSearchingEvent event) async* {
    await ordersRepository.cancelOrder(event.orderId);
    yield StoppedSearchingState();
  }
}
