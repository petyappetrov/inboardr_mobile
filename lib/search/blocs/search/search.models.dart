import 'package:flutter/material.dart';

class SearchInputs {
  final String plannedDate;
  final num sizeBoots;
  final num sizeBoard;
  final num price;
  final num comment;

  SearchInputs({
    @required this.plannedDate,
    @required this.sizeBoard,
    @required this.sizeBoots,
    @required this.price,
    this.comment,
  });

  Map<String, dynamic> toJson() {
    return {
      'plannedDate': plannedDate,
      'sizeBoots': sizeBoots,
      'sizeBoard': sizeBoard,
      'price': price,
      'comment': comment,
    };
  }
}
