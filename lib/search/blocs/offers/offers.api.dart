import 'package:inboardr_mobile/request-helper.dart';
import 'package:inboardr_mobile/search/blocs/offers/offers.models.dart';

class OffersAPIClient {
  Future<List<Offer>> getOffers(String orderId) async {
    dynamic response = await requestHelper.get('/offers?order=$orderId');
    return response
      .map<Offer>((json) => Offer.fromJson(json))
      .toList();
  }
}
