import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:inboardr_mobile/search/blocs/offers/offers.models.dart';

abstract class OffersState extends Equatable {
  @override
  List<Object> get props => [];
}

class OffersNotFoundState extends OffersState {}

class OffersFoundState extends OffersState {
  final List<Offer> offers;

  OffersFoundState({ @required this.offers });

  List<Object> get props => [offers];
}
