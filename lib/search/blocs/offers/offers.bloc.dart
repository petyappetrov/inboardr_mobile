import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:inboardr_mobile/search/blocs/offers/offers.events.dart';
import 'package:inboardr_mobile/search/blocs/offers/offers.models.dart';
import 'package:inboardr_mobile/search/blocs/offers/offers.repository.dart';
import 'package:inboardr_mobile/search/blocs/offers/offers.states.dart';
import 'package:inboardr_mobile/search/blocs/search/search.bloc.dart';
import 'package:inboardr_mobile/search/blocs/search/search.states.dart';

class OffersBloc extends Bloc<OffersEvent, OffersState> {
  final OffersRepository offersRepository;
  final SearchBloc searchBloc;
  StreamSubscription searchSubscription;

  OffersBloc({
    @required this.offersRepository,
    @required this.searchBloc,
  }) : assert(offersRepository != null) {
    searchSubscription = searchBloc.listen((SearchState state) {
      if (state is StartedSearchingState) {
        add(GetOffersEvent(orderId: state.order.id));
      }
    });
  }

  @override
  OffersState get initialState => OffersNotFoundState();

  @override
  Stream<OffersState> mapEventToState(OffersEvent event) async* {
    if (event is GetOffersEvent) {
      yield* _mapGetOffersToState(event);
    }
  }

  Stream<OffersState> _mapGetOffersToState(GetOffersEvent event) async* {
    final List<Offer> offers = await offersRepository.getOffers(event.orderId);
    if (offers.isEmpty) {
      yield OffersNotFoundState();
    } else {
      yield OffersFoundState(offers: offers);
    }
  }

  @override
  Future<void> close() {
    searchSubscription.cancel();
    return super.close();
  }
}
