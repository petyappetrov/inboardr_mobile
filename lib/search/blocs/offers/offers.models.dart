import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class Offer extends Equatable {
  final String id;
  final String order;
  final String user;
  final int snowboard;
  final num price;
  final String status;

  Offer({
    @required this.id,
    @required this.order,
    @required this.user,
    @required this.snowboard,
    @required this.price,
    @required this.status,
  });

  @override
  List<Object> get props => [
    id,
    order,
    user,
    snowboard,
    price,
    status,
  ];

  static Offer fromJson(Map<String, dynamic> json) {
    return Offer(
      id: json['_id'],
      order: json['order'],
      user: json['user'],
      snowboard: json['snowboard'],
      price: json['price'],
      status: json['status'],
    );
  }
}
