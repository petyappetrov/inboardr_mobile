import 'package:flutter/material.dart';
import 'package:inboardr_mobile/search/blocs/offers/offers.api.dart';
import 'package:inboardr_mobile/search/blocs/offers/offers.models.dart';

class OffersRepository {
  final OffersAPIClient offersAPIClient;

  OffersRepository({ @required this.offersAPIClient })
    : assert(offersAPIClient != null);

  Future<List<Offer>> getOffers(String orderId) async {
    return await offersAPIClient.getOffers(orderId);
  }
}
