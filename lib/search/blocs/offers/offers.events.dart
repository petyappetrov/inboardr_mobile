import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class OffersEvent extends Equatable {
  OffersEvent([List props = const []]) : super();
  @override
  List<Object> get props => [];
}

class GetOffersEvent extends OffersEvent {
  final String orderId;

  GetOffersEvent({ @required this.orderId });

  List<Object> get props => [orderId];

  @override
  String toString() => 'GetOffersEvent';
}
