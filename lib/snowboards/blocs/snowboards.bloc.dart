import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.events.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.models.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.repository.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.states.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

class SnowboardsBloc extends Bloc<SnowboardEvent, SnowboardsState> {
  final SnowboardsRepository snowboardsRepository;

  SnowboardsBloc({@required this.snowboardsRepository})
    : assert(snowboardsRepository != null);

  @override
  SnowboardsState get initialState => SnowboardsNotLoadedState();

  @override
  Stream<SnowboardsState> mapEventToState(SnowboardEvent event) async* {
    if (event is SnowboardsLoadEvent) {
      yield* _mapSnowboardsLoadToState();
    } else if (event is SnowboardsAddEvent) {
      yield* _mapSnowboardsAddToState(event);
    } else if (event is SnowboardsEditEvent) {
      yield* _mapSnowboardsEditToState(event);
    }
  }

  Stream<SnowboardsState> _mapSnowboardsLoadToState() async* {
    yield SnowboardsLoadingState();
    try {
      final List<Snowboard> snowboards = await snowboardsRepository.getSnowboards();
      yield SnowboardsLoadedState(snowboards: snowboards);
    } catch (error) {
      yield SnoaboardsLoadingErrorState(message: error.toString());
    }
  }

  Stream<SnowboardsState> _mapSnowboardsAddToState(SnowboardsAddEvent event) async* {
    if (state is SnowboardsLoadedState) {
      List<Asset> assets = event.inputs.photos
        .map<Asset>((item) => (item as AssetModel).asset)
        .toList();

      final SnowboardFormInputs inputs = event.inputs.copyWith(
        photos: await snowboardsRepository.uploadSnowboardPhotos(assets)
      );

      final body = jsonEncode(inputs.toJson());

      final Snowboard createdSnowboard = await snowboardsRepository.addSnowboard(body);
      final List<Snowboard> snowboards = List.from((state as SnowboardsLoadedState).snowboards)
        ..add(createdSnowboard);
      yield SnowboardsLoadedState(snowboards: snowboards);
    }
  }

  Stream<SnowboardsState> _mapSnowboardsEditToState(SnowboardsEditEvent event) async* {
    if (state is SnowboardsLoadedState) {
      List<Asset> assets = event.inputs.photos
        .where((item) => item is AssetModel)
        .map<Asset>((item) => (item as AssetModel).asset)
        .toList();

      List<ListItem> photos = event.inputs.photos
        .where((item) => item is PhotoModel)
        .toList();

      final SnowboardFormInputs inputs = event.inputs.copyWith(
        photos: [
          ...photos,
          ...await snowboardsRepository.uploadSnowboardPhotos(assets)
        ]
      );

      final body = jsonEncode(inputs.toJson());

      final Snowboard updatedSnowboard = await snowboardsRepository.editSnowboard(event.id, body);
      final List<Snowboard> snowboards = (state as SnowboardsLoadedState).snowboards
        .map((snowboard) => snowboard.id == updatedSnowboard.id ? updatedSnowboard : snowboard).toList();
      yield SnowboardsLoadedState(snowboards: snowboards);
    }
  }
}
