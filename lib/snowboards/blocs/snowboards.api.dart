import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:inboardr_mobile/request-helper.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.models.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'dart:math';

class SnowboardsAPIClient {
  Future<List<Snowboard>> getSnowboards() async {
    dynamic response = await requestHelper.get('/profile/snowboards');
    return response
      .map<Snowboard>((json) => Snowboard.fromJson(json))
      .toList();
  }

  Future<Snowboard> addSnowboard(String body) async {
    dynamic response = await requestHelper.post('/snowboards', body);
    return Snowboard.fromJson(response);
  }

  Future<Snowboard> editSnowboard(String id, String body) async {
    dynamic response = await requestHelper.put('/snowboards/$id', body);
    return Snowboard.fromJson(response);
  }

  Future<List<ListItem>> uploadSnowboardPhotos(List<Asset> photos) async {
    final multiparFiles = await Future.wait(photos.map((Asset photo) async {
      final ratio = min(
        1280 / photo.originalWidth,
        1000 / photo.originalHeight
      );
      final width = photo.originalWidth * ratio;
      final height = photo.originalHeight * ratio;
      ByteData byteData = await photo.getThumbByteData(
        width.toInt(),
        height.toInt(),
        quality: 80
      );
      List<int> imageData = byteData.buffer.asUint8List();
      MultipartFile multipartFile = MultipartFile.fromBytes(
        'files',
        imageData,
        filename: photo.name.toLowerCase(),
      );
      return multipartFile;
    }));

    final dynamic response = await requestHelper.multipartRequest('/upload', multiparFiles);
    final List<ListItem> list = response.map<ListItem>((url) {
      return PhotoModel(url: url);
    }).toList();
    return list;
  }
}
