import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

class Snowboard extends Equatable {
  final String id;
  final String titleBoard;
  final int sizeBoard;
  final String titleBoots;
  final num sizeBoots;
  final List<String> photos;

  Snowboard({
    @required this.id,
    @required this.titleBoard,
    @required this.sizeBoard,
    @required this.titleBoots,
    @required this.sizeBoots,
    @required this.photos,
  });

  @override
  List<Object> get props => [
    id,
    titleBoard,
    sizeBoard,
    titleBoots,
    sizeBoots,
    photos,
  ];

  static Snowboard fromJson(Map<String, dynamic> json) {
    return Snowboard(
      id: json['_id'],
      titleBoard: json['titleBoard'],
      sizeBoard: json['sizeBoard'],
      titleBoots: json['titleBoots'],
      sizeBoots: json['sizeBoots'],
      photos: List.from(json['photos']),
    );
  }
}

abstract class ListItem {}

class PhotoModel implements ListItem {
  final String url;
  PhotoModel({
    this.url
  });
}

class AssetModel implements ListItem {
  final Asset asset;
  AssetModel({
    this.asset
  });
}

class SnowboardFormInputs {
  final String titleBoard;
  final String titleBoots;
  final int sizeBoard;
  final num sizeBoots;
  final List<ListItem> photos;

  SnowboardFormInputs({
    this.titleBoard,
    this.titleBoots,
    this.sizeBoard,
    this.sizeBoots,
    this.photos,
  });

  SnowboardFormInputs copyWith({
    String titleBoard,
    String titleBoots,
    int sizeBoard,
    num sizeBoots,
    List<ListItem> photos,
  }) {
    return SnowboardFormInputs(
      titleBoard: titleBoard ?? this.titleBoard,
      titleBoots: titleBoots ?? this.titleBoots,
      sizeBoard: sizeBoard ?? this.sizeBoard,
      sizeBoots: sizeBoots ?? this.sizeBoots,
      photos: photos ?? this.photos,
    );
  }

  Map<String, dynamic> toJson() {
    List<String> p = photos.map((photo) => (photo as PhotoModel).url).toList();
    return {
      'titleBoard': titleBoard,
      'titleBoots': titleBoots,
      'sizeBoard': sizeBoard,
      'sizeBoots': sizeBoots,
      'photos': p,
    };
  }
}
