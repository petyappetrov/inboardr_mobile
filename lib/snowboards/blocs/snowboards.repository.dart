import 'package:flutter/material.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.api.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.models.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

class SnowboardsRepository {
  final SnowboardsAPIClient snowboardsAPIClient;

  SnowboardsRepository({ @required this.snowboardsAPIClient });

  Future<List<Snowboard>> getSnowboards() =>
    snowboardsAPIClient.getSnowboards();

  Future<List<ListItem>> uploadSnowboardPhotos(List<Asset> photos) =>
    snowboardsAPIClient.uploadSnowboardPhotos(photos);

  Future<Snowboard> addSnowboard(body) =>
    snowboardsAPIClient.addSnowboard(body);

  Future<Snowboard> editSnowboard(String id, String body) =>
    snowboardsAPIClient.editSnowboard(id, body);
}
