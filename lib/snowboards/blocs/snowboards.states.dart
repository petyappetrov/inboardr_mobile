import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.models.dart';

abstract class SnowboardsState extends Equatable {
  @override
  List<Object> get props => [];
}

class SnowboardsNotLoadedState extends SnowboardsState {}

class SnowboardsLoadingState extends SnowboardsState {}

class SnowboardsLoadedState extends SnowboardsState {
  final List<Snowboard> snowboards;
  SnowboardsLoadedState({ @required this.snowboards })
    : assert(snowboards != null);

  @override
  List<Object> get props => [snowboards];
}

class SnoaboardsLoadingErrorState extends SnowboardsState {
  final String message;
  SnoaboardsLoadingErrorState({ this.message });
}
