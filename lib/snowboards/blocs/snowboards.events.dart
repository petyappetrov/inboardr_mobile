import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.models.dart';

abstract class SnowboardEvent extends Equatable {
  SnowboardEvent([List props = const []]) : super();
  @override
  List<Object> get props => [];
}

class SnowboardsLoadEvent extends SnowboardEvent {
  SnowboardsLoadEvent();

  @override
  String toString() => 'SnowboardsLoadEvent';
}

class SnowboardsAddEvent extends SnowboardEvent {
  final SnowboardFormInputs inputs;

  SnowboardsAddEvent({ @required this.inputs });

  @override
  List<Object> get props => [
    inputs
  ];

  @override
  String toString() => 'SnowboardsAddEvent';
}

class SnowboardsEditEvent extends SnowboardEvent {
  final String id;
  final SnowboardFormInputs inputs;

  SnowboardsEditEvent({ @required this.inputs, @required this.id });

  @override
  List<Object> get props => [
    inputs
  ];

  @override
  String toString() => 'SnowboardsEditEvent';
}
