import 'package:flutter/material.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.models.dart';

class SnowboardForm extends StatefulWidget {
  final Snowboard snowboard;
  final Function submit;

  SnowboardForm({
    @required this.submit,
    this.snowboard,
    Key key
  }) : super(key: key);

  @override
  _SnowboardFormState createState() => _SnowboardFormState();
}

class _SnowboardFormState extends State<SnowboardForm> {
  List<ListItem> photos = List<ListItem>();
  final _titleBoardController = TextEditingController();
  final _titleBootsController = TextEditingController();
  int _selectedHeightIndex = 20;
  int _selectedSizeIndex = 14;
  final List<double> sizes = [for (double i = 34; i <= 46; i += 0.5) i];
  final List<int> heights = [for (int i = 130; i < 180; i += 1) i];
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    if (widget.snowboard != null) {
      _titleBoardController.text = widget.snowboard.titleBoard;
      _titleBootsController.text = widget.snowboard.titleBoots;
      int indexSelectedHeight = heights.indexOf(widget.snowboard.sizeBoard);
      int indexSelectedSize = sizes.indexOf(widget.snowboard.sizeBoots.toDouble());
      List<ListItem> list = widget.snowboard.photos
        .map<ListItem>((url) => PhotoModel(url: url))
        .toList();
      setState(() {
        photos = list;
        _selectedHeightIndex = indexSelectedHeight;
        _selectedSizeIndex = indexSelectedSize;
      });
    }
  }

  Future<void> loadAssets() async {
    List<Asset> resultList;
    String error;

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 10,
        enableCamera: true,
      );
    } on Exception catch (e) {
      error = e.toString();
    }
    if (!mounted) {
      return;
    }

    List<ListItem> newPhotos = resultList.map<ListItem>((asset) {
      return AssetModel(asset: asset);
    }).toList();

    setState(() {
      photos.addAll(newPhotos);
      if (error == null) {
        error = 'No Error Dectected';
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    double thumbWidth = (MediaQuery.of(context).size.width - 48) / 3;
    return Container(
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "Название борда",
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(0, 0, 0, 0.6),
                height: 1.3
              ),
            ),
            TextFormField(
              controller: _titleBoardController,
              decoration: InputDecoration(hintText: "Введите текст"),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Пожалуйста заполните поле';
                }
                return null;
              },
            ),
            SizedBox(height: 16),
            Row(
              children: <Widget>[
                _buildSelectHeightButton(context),
              ],
            ),
            SizedBox(height: 16),
            Text(
              "Название ботинок",
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(0, 0, 0, 0.6),
                height: 1.3
              ),
            ),
            TextFormField(
              controller: _titleBootsController,
              decoration: InputDecoration(hintText: 'Введите текст'),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Пожалуйста заполните поле';
                }
                return null;
              },
            ),
            SizedBox(height: 16),
            Row(
              children: <Widget>[
                _buildSelectSizeButton(context),
              ],
            ),
            SizedBox(height: 16),
            FormField(validator: (value) {
              if (photos.isEmpty) {
                return "Пожалуйста загрузите фото";
              }
              return null;
            }, builder: (state) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Загрузите фото",
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.w400,
                        color: Color.fromRGBO(0, 0, 0, 0.6),
                        height: 1.3),
                  ),
                  GridView.count(
                    padding: EdgeInsets.only(top: 0),
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    crossAxisCount: 3,
                    children: photosList(thumbWidth, state),
                  ),
                  SizedBox(height: 8),
                  state.hasError
                      ? Text(
                          state.errorText,
                          style: TextStyle(
                              color: Color.fromRGBO(196, 62, 56, 1),
                              fontSize: 12),
                        )
                      : Container(),
                ],
              );
            }),
            SizedBox(height: 16),
            _buildSubmitButton(context),
            SizedBox(height: 16),
          ],
        )
      ),
    );
  }

  Widget _buildSelectHeightButton(BuildContext context) {
    final FixedExtentScrollController scrollController =
        FixedExtentScrollController(initialItem: _selectedHeightIndex);
    return GestureDetector(
      onTap: () {
        showModalBottomSheet(
          context: context,
          builder: (BuildContext context) {
            return Container(
              height: 280.0,
              child: CupertinoPicker(
                squeeze: 1.1,
                scrollController: scrollController,
                backgroundColor: Colors.white,
                itemExtent: sizes.length.toDouble(),
                onSelectedItemChanged: (int index) =>
                    setState(() => _selectedHeightIndex = index),
                children:
                    List<Widget>.generate(heights.length, (int index) {
                  final num height = heights[index];
                  return Center(
                    child: Text('$height см'),
                  );
                })
              ),
            );
          });
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _buildFormLabel(label: 'Размер сноуборда'),
            _buildFormValue(value: heights[_selectedHeightIndex].toInt().toString() + ' см')
          ]
        )
      );
  }

  Widget _buildSelectSizeButton(BuildContext context) {
    final FixedExtentScrollController scrollController =
        FixedExtentScrollController(initialItem: _selectedSizeIndex);
    return GestureDetector(
      onTap: () {
        showModalBottomSheet(
          context: context,
          builder: (BuildContext context) {
            return Container(
              height: 280.0,
              child: CupertinoPicker(
                squeeze: 1.1,
                scrollController: scrollController,
                backgroundColor: Colors.white,
                itemExtent: sizes.length.toDouble(),
                onSelectedItemChanged: (int index) =>
                    setState(() => _selectedSizeIndex = index),
                children: List<Widget>.generate(sizes.length, (int index) {
                  final num size = sizes[index];
                  return Center(
                    child: Text('$size RU'),
                  );
                })
              ),
            );
          }
        );
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildFormLabel(label: 'Размер обуви'),
          _buildFormValue(value: sizes[_selectedSizeIndex].toString() + ' RU')
        ],
      ),
    );
  }

  Text _buildFormLabel({@required String label}) {
    return Text('$label:',
        style: TextStyle(
            fontSize: 20.0,
            fontWeight: FontWeight.w400,
            color: Color.fromRGBO(0, 0, 0, 0.6),
            height: 1.3));
  }

  Text _buildFormValue({@required String value}) {
    return Text(value,
        style: TextStyle(
            fontSize: 32.0, fontWeight: FontWeight.bold, height: 1.3));
  }

  SizedBox _buildSubmitButton(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 50,
      child: RaisedButton(
        child: Text("Сохранить"),
        textColor: Colors.white,
        color: Colors.lightGreen,
        onPressed: () {
          if (_formKey.currentState.validate()) {
            widget.submit(
              SnowboardFormInputs(
                photos: photos,
                titleBoard: _titleBoardController.text,
                titleBoots: _titleBootsController.text,
                sizeBoard: heights[_selectedHeightIndex],
                sizeBoots: sizes[_selectedSizeIndex],
              )
            );
          } else
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Text("Пожалуйста заполните пропущенные поля")
              )
            );
        }
      ),
    );
  }

  List<Widget> photosList(double thumbWidth, FormFieldState state) {
    return List.generate(photos.length + 1, (index) {
      if (photos.length == index) {
        return Container(
          width: thumbWidth,
          height: thumbWidth,
          child: Stack(
            children: <Widget>[
              Positioned(
                bottom: 0,
                left: 0,
                width: thumbWidth - 12,
                height: thumbWidth - 12,
                child: GestureDetector(
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.black12,
                      border: Border.all(
                        width: 1,
                        color: state.hasError
                          ? Color.fromRGBO(196, 62, 56, 1)
                          : Colors.transparent
                      ),
                    ),
                    child: Center(
                      child: Icon(
                        Icons.add,
                        size: 48,
                        color: Color.fromRGBO(0, 0, 0, 0.2),
                      )
                    ),
                  ),
                  onTap: loadAssets,
                ),
              ),
            ],
          ),
        );
      }
      return Container(
        width: thumbWidth,
        height: thumbWidth,
        child: Stack(
          children: <Widget>[
            Positioned(
              bottom: 0,
              left: 0,
              width: thumbWidth - 12,
              height: thumbWidth - 12,
              child: Container(
                child: photos[index] is AssetModel
                  ? AssetThumb(
                      asset: (photos[index] as AssetModel).asset,
                      width: 300,
                      height: 300,
                    )
                  : Image.network(
                    (photos[index] as PhotoModel).url,
                    fit: BoxFit.cover,
                  ),
                ),
            ),
            Positioned(
              right: 0,
              top: 0,
              child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                child: Container(
                  width: 24,
                  height: 24,
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(13)),
                  child: Icon(Icons.remove, size: 24, color: Colors.white),
                ),
                onTap: () {
                  setState(() {
                    photos.removeAt(index);
                  });
                }),
            )
          ],
        ),
      );
    });
  }
}
