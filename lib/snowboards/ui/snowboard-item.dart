import 'package:flutter/material.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.models.dart';
import 'package:inboardr_mobile/ui/card.dart';

class SnowboardItem extends StatelessWidget {
  final Snowboard snowboard;
  final Function onTap;

  SnowboardItem({
    Key key,
    @required this.snowboard,
    @required this.onTap
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CardWithBoxShadow(
      padding: EdgeInsets.all(16.0),
      onTap: onTap,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildImage(),
          _buildInfo()
        ],
      ),
    );
  }

  Widget _buildImage() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10.0),
      child: Hero(
        tag: snowboard.id,
        child: Image.network(
          snowboard.photos[0],
          width: 100,
          height: 100,
          fit: BoxFit.cover,
        ),
      )
    );
  }

  Widget _buildInfo() {
    return Padding(
      padding: EdgeInsets.only(left: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            snowboard.titleBoard,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16.0
            )
          ),
          Text(
            'Размер: ' + snowboard.sizeBoard.toString(),
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 14),
          Text(
            'Ботинки: \n' + snowboard.titleBoots,
            style: TextStyle(
              // fontWeight: FontWeight.bold,
              // fontSize: 16.0
            )
          ),
          Text('Размер: ' + snowboard.sizeBoots.toString())
        ],
      ),
    );
  }
}
