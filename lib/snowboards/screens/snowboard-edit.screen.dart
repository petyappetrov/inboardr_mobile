import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:inboardr_mobile/ui/top-navigation.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.bloc.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.events.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.states.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.models.dart';
import 'package:inboardr_mobile/snowboards/ui/snowboard-form.dart';

class SnowboardEditScreen extends StatelessWidget {
  final Snowboard snowboard;
  const SnowboardEditScreen({
    @required this.snowboard,
    Key key
    }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<SnowboardsBloc, SnowboardsState>(
      listener: (BuildContext context, SnowboardsState state) {
        if (state is SnowboardsLoadedState) {
          Navigator.popUntil(context, (route) => route.isFirst);
        }
      },
      child: Scaffold(
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                TopNavigation(title: 'Редактировать\nсноуборд'),
                Container(
                    padding: EdgeInsets.fromLTRB(24.0, 24.0, 24.0, 0.0),
                    child: SnowboardForm(
                      snowboard: snowboard,
                      submit: (SnowboardFormInputs inputs) {
                        BlocProvider.of<SnowboardsBloc>(context)
                          .add(SnowboardsEditEvent(id: snowboard.id, inputs: inputs));
                      },
                    )
                        ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
