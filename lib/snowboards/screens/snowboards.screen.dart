import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:inboardr_mobile/snowboards/screens/snowboard-add.screen.dart';
import 'package:inboardr_mobile/snowboards/screens/snowboard.screen.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.bloc.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.events.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.models.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.states.dart';
import 'package:inboardr_mobile/snowboards/ui/snowboard-item.dart';
import 'package:inboardr_mobile/ui/top-navigation.dart';

class SnowboardsScreen extends StatefulWidget {
  const SnowboardsScreen({Key key}) : super(key: key);

  @override
  _SnowboardsScreenState createState() => _SnowboardsScreenState();
}

class _SnowboardsScreenState extends State<SnowboardsScreen>
  with SingleTickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  AnimationController _animationController;
  Animation<Offset> _slideAnimationOffset;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 0),
      value: 1,
    );
    _slideAnimationOffset = Tween<Offset>(
      begin: Offset(0, -0.30),
      end: Offset(0.0, 0.0),
    ).animate(_animationController);
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  bool _scrollListener(ScrollNotification scrollInfo) {
    if (scrollInfo.metrics.axis == Axis.vertical) {
      _animationController.animateTo((60 - scrollInfo.metrics.pixels) / 60);
      return true;
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Navigator(
      observers: [
        HeroController()
      ],
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute(
          settings: settings,
          builder: (BuildContext context) {
            return BlocBuilder<SnowboardsBloc, SnowboardsState>(
              builder: (BuildContext context, SnowboardsState state) {
                if (state is SnowboardsNotLoadedState) {
                  BlocProvider.of<SnowboardsBloc>(context).add(SnowboardsLoadEvent());
                  return Container(
                    color: Color.fromRGBO(242, 243, 246, 1),
                    child: Center(child: CircularProgressIndicator())
                  );
                }

                if (state is SnowboardsLoadingState) {
                  return Container(
                    color: Color.fromRGBO(242, 243, 246, 1),
                    child: Center(child: CircularProgressIndicator())
                  );
                }

                List<Snowboard> snowboards = (state as SnowboardsLoadedState).snowboards;

                return Scaffold(
                  floatingActionButton: FloatingActionButton(
                    backgroundColor: Colors.lightGreen,
                    child: Icon(Icons.add),
                    onPressed: () {
                      Navigator.push(context,
                        MaterialPageRoute(builder: (context) => SnowboardAddScreen())
                      );
                    },
                  ),
                  body: SafeArea(
                    child: NotificationListener<ScrollNotification>(
                      onNotification: _scrollListener,
                      child: Stack(children: <Widget>[
                        FadeTransition(
                          opacity: _animationController,
                          child: SlideTransition(
                            position: _slideAnimationOffset,
                            child: TopNavigation(
                              title: 'Сноуборды',
                            ),
                          ),
                        ),
                        SingleChildScrollView(
                          child: Container(
                            padding: EdgeInsets.fromLTRB(24.0, 72.0, 24.0, 24.0),
                            width: double.infinity,
                            child: ListView(
                              padding: EdgeInsets.all(0),
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              children: snowboards.map<Widget>((Snowboard snowboard) {
                                return SnowboardItem(
                                  snowboard: snowboard,
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) =>
                                        SnowboardScreen(snowboard: snowboard)
                                      ),
                                    );
                                  },
                                );
                              }
                            ).toList()),
                          ),
                        )
                      ]),
                    ),
                  )
                );
              },
            );
          }
        );
      },
    );
  }
}
