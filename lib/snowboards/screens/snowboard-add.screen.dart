import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.bloc.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.events.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.models.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.states.dart';
import 'package:inboardr_mobile/snowboards/ui/snowboard-form.dart';
import 'package:inboardr_mobile/ui/top-navigation.dart';

class SnowboardAddScreen extends StatelessWidget {
  const SnowboardAddScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<SnowboardsBloc, SnowboardsState>(
      listener: (BuildContext context, SnowboardsState state) {
        if (state is SnowboardsLoadedState){
          Navigator.of(context).pop();
        }
      },
      child: Scaffold(
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                TopNavigation(title: 'Добавить\nсноуборд'),
                Container(
                  padding: EdgeInsets.fromLTRB(24.0, 24.0, 24.0, 0.0),
                  child: SnowboardForm(
                    submit: (SnowboardFormInputs inputs) {
                      BlocProvider.of<SnowboardsBloc>(context)
                        .add(SnowboardsAddEvent(inputs: inputs));
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
