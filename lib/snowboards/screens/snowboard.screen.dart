import 'package:flutter/material.dart';
import 'package:inboardr_mobile/snowboards/screens/snowboard-edit.screen.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.models.dart';
import 'package:inboardr_mobile/ui/animation-child.dart';

class SnowboardScreen extends StatelessWidget {
  final Snowboard snowboard;
  SnowboardScreen({
    Key key,
    @required this.snowboard,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.lightGreen,
        child: Icon(Icons.edit),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => SnowboardEditScreen(snowboard: snowboard,)),
          );
        },
      ),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            iconTheme: IconThemeData(color: Colors.black),
            brightness: Brightness.light,
            leading: AnimationChild(
              delay: Duration(milliseconds: 250),
              fadingDuration: Duration(milliseconds: 300),
              child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(Icons.arrow_back_ios),
              ),
            ),
            expandedHeight: 300,
            floating: false,
            backgroundColor: Color.fromRGBO(242, 243, 246, 1),
            elevation: 0,
            flexibleSpace: FlexibleSpaceBar(
              background: _buildMainPhoto(),
            ),
          ),
          SliverFillRemaining(
            child: Container(
              padding: EdgeInsets.fromLTRB(24.0, 24.0, 24.0, 60.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Ботинки: ' + snowboard.titleBoots),
                  Text('Размер: ' + snowboard.sizeBoots.toString()),
                ],
              ),
            ),
          )
        ],
      )
    );
  }

  Stack _buildMainPhoto() {
    return Stack(children: <Widget>[
      Hero(
        tag: snowboard.id,
        child: Image.network(
          snowboard.photos[0],
          height: 350,
          width: double.infinity,
          fit: BoxFit.cover,
        )
      ),
      Positioned(
        bottom: 24.0,
        left: 24.0,
        child: AnimationChild(
          delay: Duration(milliseconds: 250),
          fadingDuration: Duration(milliseconds: 300),
          child: Container(
            padding: EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
            decoration: BoxDecoration(color: Colors.black, borderRadius: BorderRadius.circular(6.0)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  snowboard.titleBoard,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 22.0,
                    fontWeight: FontWeight.bold,
                  )
                ),
                SizedBox(height: 4),
                Text(
                  'Размер: ' + snowboard.sizeBoard.toString(),
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18.0,
                  )
                )
              ],
            ),
          )
        ),
      )
    ]);
  }
}
