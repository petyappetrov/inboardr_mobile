import 'package:flutter/material.dart';

class UserCard extends StatelessWidget {
  final String firstName;
  final String lastName;
  final String phone;
  final String avatar;

  UserCard({
    @required this.firstName,
    @required this.lastName,
    @required this.phone,
    this.avatar,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        children: <Widget>[
          avatar != null
            ? CircleAvatar(
              radius: 60,
              backgroundImage: NetworkImage(avatar)
            )
            : Icon(Icons.account_circle, size: 120),
          SizedBox(height: 4),
          Text(
            '$firstName $lastName',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 26),
          ),
          SizedBox(height: 8),
          Text(
            phone,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ),
        ],
      ),
    );
  }
}
