import 'package:flutter/material.dart';

class TopNavigation extends StatelessWidget {
  final String title;

  const TopNavigation({
    Key key,
    @required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(24, 8, 24, 0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(
              fontWeight: FontWeight.w900,
              fontSize: 32,
              color: Colors.black
            ),
          ),
          _buildNotificationsButton(context)
        ],
      ),
    );
  }

  Widget _buildNotificationsButton(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, '/notifications');
      },
      child: Padding(
        padding: const EdgeInsets.only(top: 6.0),
        child: Stack(
          overflow: Overflow.visible,
          children: <Widget>[
            Icon(
              Icons.notifications,
              color: Colors.black,
              size: 28,
            ),
            Positioned(
              right: -7,
              top: -7,
              child: Container(
                width: 20,
                height: 20,
                padding: EdgeInsets.all(1),
                decoration: BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.circular(13),
                ),
                child: Center(
                  child: Text(
                    '8',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            )
          ],
        ),
      )
    );
  }
}
