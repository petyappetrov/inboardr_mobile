import 'package:flutter/material.dart';
import 'package:inboardr_mobile/root/blocs/destinations/destinations.models.dart';

class BottomNavigation extends StatelessWidget {
  final int currentIndex;
  final List<Destination> destinations;
  final Function setIndex;
  BottomNavigation({
    @required this.currentIndex,
    @required this.destinations,
    @required this.setIndex,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(
          top: BorderSide(width: 1.0, color: Color.fromRGBO(0, 0, 0, 0.05)
        ),
      ),
      ),
      height: 90.0,
      padding: EdgeInsets.symmetric(horizontal: 8.0),
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: destinations.map((destination) {
          final int index = destinations.indexOf(destination);
          final bool isSelected = currentIndex == index;
          return GestureDetector(
            onTap: () => setIndex(index),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              physics: NeverScrollableScrollPhysics(),
              child: Container(
                width: (MediaQuery.of(context).size.width - 32) / 4,
                padding: EdgeInsets.only(bottom: 32),
                child: isSelected
                  ? (
                    Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.fromLTRB(0, 16, 0, 2),
                            child: Text(destination.title, style: TextStyle(fontWeight: FontWeight.w900, fontSize: 13)),
                          ),
                          SizedBox(height: 16, child: Icon(Icons.brightness_1, size: 8, color: Colors.black)),
                        ],
                      ),
                    )
                  )
                  : (
                    Center(
                      child: Icon(destination.icon, color: Colors.black87)
                    )
                  )
              ),
            ),
          );
        }).toList()
      )
    );
  }
}
