import 'package:flutter/material.dart';

class CardWithBoxShadow extends StatelessWidget {
  final Widget child;
  final Function onTap;
  final EdgeInsets padding;
  CardWithBoxShadow({
    Key key,
    @required this.child,
    this.onTap,
    this.padding = const EdgeInsets.all(24.0),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          color: Color.fromRGBO(0, 0, 0, 0.02),
          blurRadius: 25.0,
          spreadRadius: 8.0,
          offset: Offset(0.0, 0.0),
        )
      ]),
      child: Card(
        margin: EdgeInsets.fromLTRB(0, 0, 0, 16),
        clipBehavior: Clip.antiAlias,
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        child: Stack(
          children: <Widget>[
            Container(
              padding: padding,
              child: child,
            ),
            onTap is Function
              ? Positioned.fill(child: InkWell(onTap: onTap))
              : Container()
          ],
        )
      ),
    );
  }
}
