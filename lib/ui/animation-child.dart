import 'dart:async';
import 'package:flutter/material.dart';

class AnimationChild extends StatefulWidget {
  final Widget child;
  final Duration delay;
  final Duration fadingDuration;

  const AnimationChild({
    @required this.child,
    this.delay,
    this.fadingDuration = const Duration(milliseconds: 800),
  });

  @override
  _AnimationChildState createState() => _AnimationChildState();
}

class _AnimationChildState extends State<AnimationChild> with TickerProviderStateMixin {
  AnimationController _animationController;

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
      vsync: this,
      duration: widget.fadingDuration,
    );

    if (widget.delay == null) {
      _animationController.forward();
    } else {
      Timer(widget.delay, () => _animationController.forward());
    }
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FadeTransition(
      child: widget.child,
      opacity: _animationController,
    );
  }
}
