import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:inboardr_mobile/orders/blocs/orders.api.dart';
import 'package:inboardr_mobile/orders/blocs/orders.repository.dart';
import 'package:inboardr_mobile/search/blocs/offers/offers.api.dart';
import 'package:inboardr_mobile/search/blocs/offers/offers.bloc.dart';
import 'package:inboardr_mobile/search/blocs/offers/offers.repository.dart';
import 'package:inboardr_mobile/search/blocs/search/search.bloc.dart';
import 'package:inboardr_mobile/root/blocs/destinations/destinations.states.dart';
import 'package:inboardr_mobile/root/blocs/destinations/destinations.bloc.dart';
import 'package:inboardr_mobile/root/blocs/mode/mode.models.dart';
import 'package:inboardr_mobile/root/blocs/mode/mode.bloc.dart';
import 'package:inboardr_mobile/root/blocs/mode/mode.states.dart';
import 'package:inboardr_mobile/root/blocs/mode/mode.events.dart';
import 'package:inboardr_mobile/root/blocs/mode/mode.repository.dart';
import 'package:inboardr_mobile/root/root.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.api.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.bloc.dart';
import 'package:inboardr_mobile/snowboards/blocs/snowboards.repository.dart';
import 'package:inboardr_mobile/authentication/blocs/authentication.events.dart';
import 'package:inboardr_mobile/authentication/blocs/authentication.api.dart';
import 'package:inboardr_mobile/authentication/blocs/authentication.bloc.dart';
import 'package:inboardr_mobile/authentication/blocs/authentication.repository.dart';
import 'package:inboardr_mobile/authentication/blocs/authentication.states.dart';
import 'package:inboardr_mobile/authentication/screens/welcome.screen.dart';
import 'package:inboardr_mobile/authentication/screens/login.screen.dart';
import 'package:inboardr_mobile/authentication/screens/splash.screen.dart';

class SimpleBlocDelegate extends BlocDelegate {
  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
    print(event);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print(transition);
  }

  @override
  void onError(Bloc bloc, Object error, StackTrace stacktrace) {
    super.onError(bloc, error, stacktrace);
    print(error);
  }
}

void main() {
  /// Репозиторий режима приложения
  final ModeRepository modeRepository = ModeRepository();

  /// Репозиторий упарвления токеном авторизации
  final AuthenticationRepository authenticationRepository = AuthenticationRepository(
    authenticationAPIClient: AuthenticationAPIClient()
  );

  /// Репозиторий для внешнего упарвления страницы сноубородов
  final SnowboardsRepository snowboardsRepository = SnowboardsRepository(
    snowboardsAPIClient: SnowboardsAPIClient()
  );

  /// Репозиторий для заказов
  final OrdersRepository ordersRepository = OrdersRepository(
    ordersAPIClient: OrdersAPIClient()
  );

  /// Репозиторий для предложений
  final OffersRepository offersRepository = OffersRepository(
    offersAPIClient: OffersAPIClient()
  );

  /// Логирование переходов состояний приложения
  BlocSupervisor.delegate = SimpleBlocDelegate();

  /// Выставление по-умолчанию языка для Intl пакета
  Intl.defaultLocale = 'ru';

  /// Выставление цвета статусбара
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: Colors.black,
      statusBarBrightness: Brightness.light,
    )
  );

  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider<AuthenticationBloc>(
          create: (context) => AuthenticationBloc(
            authenticationRepository: authenticationRepository,
          )..add(AuthenticationApplicationStartedEvent()),
        ),
        BlocProvider<ModeBloc>(
          create: (context) => ModeBloc(
            modeRepository: modeRepository,
            authenticationBloc: BlocProvider.of<AuthenticationBloc>(context)
          )..add(InitializeModeEvent()),
        ),
        BlocProvider<DestinationBloc>(
          create: (context) => DestinationBloc(
            modeBloc: BlocProvider.of<ModeBloc>(context),
          )
        ),
        BlocProvider<SnowboardsBloc>(
          create: (context) => SnowboardsBloc(
            snowboardsRepository: snowboardsRepository,
          )
        ),
        BlocProvider<SearchBloc>(
          create: (context) => SearchBloc(
            ordersRepository: ordersRepository,
          )
        ),
        BlocProvider<OffersBloc>(
          create: (context) => OffersBloc(
            offersRepository: offersRepository,
            searchBloc: BlocProvider.of<SearchBloc>(context),
          )
        )
      ],
      child: InBoardr(),
    )
  );
}

class InBoardr extends StatelessWidget {
  InBoardr({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: new ThemeData(
        brightness: Brightness.light,
      ),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        Locale('ru'),
        Locale('en'),
      ],
      debugShowCheckedModeBanner: false,
      title: 'inBoardr App',
      home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
        builder: (BuildContext context, AuthenticationState state) {
          if (state is AuthenticationUninitializedState) {
            return SplashScreen();
          }
          if (state is AuthenticationLoadingState) {
            return Container(color: Colors.white, child: Center(child: CircularProgressIndicator()));
          }
          if (state is AuthenticationUnauthenticatedState) {
            return WelcomeScreen();
          }
          if (state is AuthenticationLoadingErrorState) {
            return LoginScreen();
          }
          if (state is AuthenticationAuthenticatedState) {
            return BlocBuilder<ModeBloc, ModeState>(
              builder: (context, state) {
                if (state is ModeLoadingState) {
                  return SplashScreen();
                }
                Modes mode = (state as ModeLoadedState).mode;
                return BlocBuilder<DestinationBloc, DestinationState>(
                  builder: (context, s) {
                    int destination = (s as DestinationChangedState).destination;
                    return Root(
                      mode: mode,
                      destination: destination
                    );
                  },
                );
              }
            );
          }
          return SplashScreen();
        },
      )
    );
  }
}
